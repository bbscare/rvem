/*
 * File: TraceData.rs
 * Project: src
 * Created Date: Friday February 25th 2022
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 25th February 2022 11:57:30 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2022 INRIA
 */
use std::num::Wrapping;

#[derive(Debug,Clone)]
pub struct TraceData {
    pub trace: Vec<u32>,
    pub input: Vec<Wrapping<u32>>,
    pub output: Vec<Wrapping<u32>>
}

pub struct TraceDataChunk {
    pub traces: Vec<Vec<u32>>,
    pub inputs: Vec<Vec<Wrapping<u32>>>,
    pub outputs: Vec<Vec<Wrapping<u32>>>
}