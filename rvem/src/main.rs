


#[macro_use] extern crate clap;
#[macro_use] extern crate failure;
#[macro_use] extern crate log;
extern crate fern;
extern crate chrono;

extern crate rv_lib;
extern crate simplass;

pub mod iofile;
use crate::iofile::IOFile;
pub mod trace_file;
use crate::trace_file::TraceFile;
pub mod trace_data;
use crate::trace_data::{TraceData, TraceDataChunk};

use chrono::Local;
use failure::Error;
use clap::{App, ArgMatches};
// use byteorder::{ WriteBytesExt, LittleEndian};

use std::fs;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::io;
use std::io::Write;

use std::sync::mpsc::{Sender, Receiver};
use std::sync::mpsc;
use std::thread;
use std::thread::JoinHandle;


use std::num::Wrapping;


use rv_lib::{
    machine::*,
    instructions::*,
    program::*,
};

pub const START_ADDRESS: MemoryAddress = MemoryAddress(0x8000);

fn init_log_file(log_filename: &str) -> Result<(), Error> {
    let _ = fs::create_dir("log").map_err(|e|format_err!("Cannot create dir: {}", e));

    //remove file if exists
    let _ = fs::remove_file(&log_filename).map_err(|e|format_err!("Cannot remove file: {}", e));
    Ok(())
}

fn init_logger(verbosity: log::LevelFilter, log_verbosity: log::LevelFilter) -> Result<(), Error> {
    let log_filename = format!("log/out.log");
    let base_config = fern::Dispatch::new()
    .format(|out, message, record| {
        //out.finish(format_args!("{}[{}][{}] {}",
        out.finish(format_args!("{}[{}] {}",
            Local::now()
                .format("[%Y-%m-%d][%H:%M:%S]"),
            //record.target(),
            record.level(),
            message))
    });

    let stdout_log = fern::Dispatch::new()
        .level(verbosity)
        .chain(std::io::stdout());

    if log_verbosity != log::LevelFilter::Off {

        init_log_file(&log_filename)?;

        let file_log = fern::Dispatch::new()
            .level(log_verbosity)
            .chain(fern::log_file(log_filename).map_err(|e|format_err!("{}", e))?);

        base_config
            .chain(file_log)
            .chain(stdout_log)
            .apply()
            .map_err(|e|format_err!("{}", e))?;

    }
    else {
        base_config
            .chain(stdout_log)
            .apply()
            .map_err(|e|format_err!("{}", e))?;
    }

    Ok(())
}

fn verbosity_level(n: u64) -> log::LevelFilter {
    match n {
        1 => log::LevelFilter::Error,
        2 => log::LevelFilter::Warn,
        3 => log::LevelFilter::Info,
        4 => log::LevelFilter::Debug,
        5 => log::LevelFilter::Trace,
        _ => log::LevelFilter::Off
    }
}

fn extract_input_path(matches: &ArgMatches) -> Result<PathBuf, Error> {

    if let Some(prog_path) = matches.value_of("input") {
        return Ok(PathBuf::from(prog_path));
    }

    Err(format_err!("No input file specified."))
}

fn extract_io_path(matches: &ArgMatches) -> Result<PathBuf, Error> {

    if let Some(prog_path) = matches.value_of("output") {
        return Ok(PathBuf::from(prog_path));
    }

    Err(format_err!("No IO file specified."))
}

fn extract_trace_output_path(matches: &ArgMatches) -> Result<PathBuf, Error> {

    if let Some(prog_path) = matches.value_of("trace_output") {
        return Ok(PathBuf::from(prog_path));
    }

    Err(format_err!("No trace output file specified."))
}


fn main() -> Result<(), Error> {

    let cliyaml = load_yaml!("cli.yml");
    let matches = App::from_yaml(cliyaml).get_matches();

    let verbosity = verbosity_level(matches.occurrences_of("verbose"));
    let log_verbosity = verbosity_level(matches.occurrences_of("logverbose"));

    
    init_logger(verbosity, log_verbosity)?;

    trace!("Logger initialized...");
    let input_file = extract_input_path(&matches)?;
    let output_file = extract_io_path(&matches)?;

    //run a program
    if let Some(_) = matches.subcommand_matches("run") {
        run_prog(&input_file, output_file)?;
    }
    //trace a program
    else if let Some(trace_matches) = matches.subcommand_matches("trace") {
        let trace_output_file = extract_trace_output_path(&matches)?;
        let trace_count: u32 = if let Some(count) = trace_matches.value_of("count") {
            u32::from_str(count)?
        }
        else {
            100u32
        };

        

        run_trace(&input_file, output_file, trace_output_file, trace_count)?;
    }
    
    Ok(())
}

fn run_prog(pb: &Path, io_path: PathBuf) -> Result<(), Error> {
    info!("Starting compilation...");
    let postlayout = simplass::compiler::PostLayout::compile(pb, None)?;
    info!("Program {} compiled!", pb.display());

    info!("Setting up machine...");
    let mut machine: Core<RV32I, MemoryArray1M> = Core::new(START_ADDRESS);
    for mem_chunk in postlayout.chunks.into_iter() {
        info!("A new chunk has been written in memory {:x}", mem_chunk.start_address);
        machine.write_memory_chunk(mem_chunk)?;
    }
    info!("Machine ready to start.");
    info!("Machine starting...");
    
    let mut listing = Listing::new();
    listing.read_symbol_ledger(&postlayout.ledger.symbols);

    let input_file_path = io_path.with_extension("in");
    let mut input_io = IOFile::new();
    let output_file_path = io_path.with_extension("out");
    let mut output_io = IOFile::new();

    loop/*for _ in 0..1000*/ {
        let step_res = machine.step(Some(&mut listing));

        match step_res {
            Err(e) => { // end of run
                let _ = fs::create_dir("out").map_err(|e|format_err!("Cannot create dir: {}", e));
                listing.write_to_file("out/listing.lst")?;
                input_io.push_trace_wrapping(&machine.io().inputs);
                output_io.push_trace_wrapping(&machine.io().outputs);

                input_io.write_file(&input_file_path)?;
                output_io.write_file(&output_file_path)?;

                return Err(e.into());
            },
            _ => {} // continue
        }
    }
}

fn trace_writing_thread(path: PathBuf, rx: Receiver<Vec<u32>>, trace_len: u32, trace_count: u32) -> JoinHandle<()>{
    let child = thread::spawn(move || {

        let mut trace_file = TraceFile::new(path).unwrap();
        let mut current_count: u32 = 0;

        trace_file.write_header(trace_len, trace_count).unwrap();
        loop {
            match rx.recv() {
                Ok(new_trace) => {
                    trace_file.push_trace(&new_trace);
                    trace_file.write_buffer().unwrap();
                    current_count += 1;

                    if current_count == trace_count {
                        break;
                    }
                },
                Err(_) => {
                    break;
                }
            }
        }
    });
    child
}

fn trace_writing_thread_vec(path: PathBuf, rx: Receiver<Vec<Vec<u32>>>, trace_len: u32, trace_count: u32) -> JoinHandle<()>{
    let child = thread::spawn(move || {

        let mut trace_file = TraceFile::new(path).unwrap();
        let mut current_count: u32 = 0;

        trace_file.write_header(trace_len, trace_count).unwrap();
        loop {
            match rx.recv() {
                Ok(new_traces) => {
                    let count = new_traces.len() as u32;
                    for new_trace in new_traces {
                        trace_file.push_trace(&new_trace);
                    }
                    trace_file.write_buffer().unwrap();
                    current_count += count;

                    if current_count == trace_count {
                        break;
                    }
                },
                Err(_) => {
                    break;
                }
            }
        }
    });
    child
}

fn trace_wrapping_writing_thread(path: PathBuf, rx: Receiver<Vec<Wrapping<u32>>>, trace_len: u32, trace_count: u32) -> JoinHandle<()>{
    let child = thread::spawn(move || {

        let mut trace_file = TraceFile::new(path).unwrap();
        let mut current_count: u32 = 0;

        trace_file.write_header(trace_len, trace_count).unwrap();
        loop {
            match rx.recv() {
                Ok(new_trace) => {
                    trace_file.push_trace_wrapping(&new_trace);
                    trace_file.write_buffer().unwrap();
                    current_count += 1;

                    if current_count == trace_count {
                        break;
                    }
                },
                Err(_) => {
                    break;
                }
            }
        }
    });
    child
}

fn trace_wrapping_writing_thread_vec(path: PathBuf, rx: Receiver<Vec<Vec<Wrapping<u32>>>>, trace_len: u32, trace_count: u32) -> JoinHandle<()> {
    let child = thread::spawn(move || {

        let mut trace_file = TraceFile::new(path).unwrap();
        let mut current_count: u32 = 0;

        trace_file.write_header(trace_len, trace_count).unwrap();
        loop {
            match rx.recv() {
                Ok(new_traces) => {
                    let count = new_traces.len() as u32;
                    for trace in new_traces {
                        trace_file.push_trace_wrapping(&trace);
                    }
                    trace_file.write_buffer().unwrap();
                    current_count += count;

                    if current_count == trace_count {
                        break;
                    }
                },
                Err(_) => {
                    break;
                }
            }
        }
    });
    child
}

fn compute_traces(postlayout: simplass::compiler::PostLayout, trace_count: u32, trace_len: u32, tx: Sender<TraceDataChunk>) {
    let block_size = 100;
    assert!(trace_count % block_size == 0);

    let _child = thread::spawn(move || {

        let block_count = trace_count / block_size;
        for _ in 0..block_count {
            let mut traces: Vec<Vec<u32>> = Vec::new();
            let mut inputs: Vec<Vec<Wrapping<u32>>> = Vec::new();
            let mut outputs: Vec<Vec<Wrapping<u32>>> = Vec::new();

            for _ in 0..block_size {
                //rebuild machine
                let mut machine: Core<RV32I, MemoryArray1M> = Core::new(START_ADDRESS);
                for mem_chunk in postlayout.chunks.iter() {
                    info!("A new chunk has been written in memory {:x}", mem_chunk.start_address);
                    machine.write_memory_chunk(mem_chunk.clone()).unwrap();
                }
                let mut trace: Vec<u32> = Vec::new();
                // one trace execution
                loop {
                    let step_res = machine.tracing_step(None);
                    
        
                    match step_res {
                        Ok(r_opt) => {
                            if let Some(u) = r_opt {
                                trace.push(u);
                            }
                        },
                        Err(_) => { //end of this run
        
                            //verify trace size
                            if ((trace.len()  as u32) << 2) != trace_len {
                                panic!("Incorrect trace size");
                                // return Err(format_err!("Incorrect trace ({}) size {} instead of {}.", i, trace.len() << 2, trace_len));
                            }
                        
                            //send to writers
                            traces.push(trace);
                            inputs.push(machine.io().inputs.clone());
                            outputs.push(machine.io().outputs.clone());
                            // tx_trace.send(trace)?;
                            // tx_input.send(machine.io().inputs.clone())?;
                            // tx_output.send(machine.io().outputs.clone())?;
        
                            break;
                        },
                    }
                    
                }
    

            }

            let chunk = TraceDataChunk { traces, inputs, outputs };

            //send block
            tx.send(chunk).unwrap();
        }
         
    });
}

fn run_trace(prog: &Path, io_path: PathBuf, trace_output_path: PathBuf, trace_count: u32) -> Result<(), Error> {
    info!("Starting compilation...");
    let postlayout = simplass::compiler::PostLayout::compile(prog, None)?;
    info!("Program {} compiled!", prog.display());
    


    // do a dummy run to evaluate the sizes of the traces
    let mut dummy_machine: Core<RV32I, MemoryArray1M> = Core::new(START_ADDRESS);
    for mem_chunk in postlayout.chunks.iter() {
        info!("A new chunk has been written in memory {:x}", mem_chunk.start_address);
        dummy_machine.write_memory_chunk(mem_chunk.clone())?;
    }
    let mut dummy_trace: Vec<u32> = Vec::new();
    loop {
        let step_res = dummy_machine.tracing_step(None);
        

        match step_res {
            Ok(r_opt) => {
                if let Some(u) = r_opt {
                    dummy_trace.push(u);
                }
            },
            Err(_) => { 
                break;
            },
        }
    }



    let trace_len: u32 = dummy_trace.len() as u32 * 4;
    dummy_trace.clear();

    let input_len: u32 = dummy_machine.io().inputs.len() as u32 * 4;
    let output_len: u32 = dummy_machine.io().outputs.len() as u32 * 4;

    let input_file_path = io_path.with_extension("in");
    let output_file_path = io_path.with_extension("out");

    let (tx_trace, rx_trace) = mpsc::channel();
    let (tx_input, rx_input) = mpsc::channel();
    let (tx_output, rx_output) = mpsc::channel();

    let t1 = trace_writing_thread_vec(trace_output_path, rx_trace, trace_len, trace_count);
    let t2 = trace_wrapping_writing_thread_vec(input_file_path, rx_input, input_len, trace_count);
    let t3 = trace_wrapping_writing_thread_vec(output_file_path, rx_output, output_len, trace_count);

    const THREAD_COUNT: u32 = 2;
    let mut compute_channels: Vec<Receiver<TraceDataChunk>> = Vec::new();

    for _ in 0..THREAD_COUNT {
        let (tx_compute, rx_compute) = mpsc::channel();
        compute_channels.push(rx_compute);
        compute_traces(postlayout.clone(), trace_count/THREAD_COUNT, trace_len, tx_compute);
    }

    let mut end_condition = false;
    let mut counter: u32 = 0;

    while !end_condition {
        for rx in compute_channels.iter() {
            match rx.recv() {
                Ok(trace_data_chunk) => {
                    let count = trace_data_chunk.traces.len() as u32;

                    tx_trace.send(trace_data_chunk.traces)?;
                    tx_input.send(trace_data_chunk.inputs)?;
                    tx_output.send(trace_data_chunk.outputs)?;
                    
                    counter += count;

                    if (counter) % 100 == 0 {
                        print!("\rTrace {}/{} done.", counter, trace_count);
                        io::stdout().flush().unwrap();
                    }

                    if counter == trace_count {
                        // println!("Leaving");
                        end_condition = true;
                        break;
                    }
                },
                Err(e) => {
                    println!("An error occured in run_trace: {:?}", e);
                    end_condition = true;
                    break;
                }
            }
        }
    }

    t1.join().unwrap();
    t2.join().unwrap();
    t3.join().unwrap();

    // println!("Threads joined");

    // for i in 0..trace_count {

    //     //rebuild machine
    //     let mut machine: Core<RV32I, MemoryArray1M> = Core::new(START_ADDRESS);
    //     for mem_chunk in postlayout.chunks.iter() {
    //         info!("A new chunk has been written in memory {:x}", mem_chunk.start_address);
    //         machine.write_memory_chunk(mem_chunk.clone())?;
    //     }
    //     let mut trace: Vec<u32> = Vec::new();
    //     // one trace execution
    //     loop {
    //         let step_res = machine.tracing_step(None);
            

    //         match step_res {
    //             Ok(r_opt) => {
    //                 if let Some(u) = r_opt {
    //                     trace.push(u);
    //                 }
    //             },
    //             Err(_) => { //end of this run

    //                 //verify trace size
    //                 if ((trace.len()  as u32) << 2) != trace_len {
    //                     return Err(format_err!("Incorrect trace ({}) size {} instead of {}.", i, trace.len() << 2, trace_len));
    //                 }
                   
    //                 //send to writers
    //                 tx_trace.send(trace)?;
    //                 tx_input.send(machine.io().inputs.clone())?;
    //                 tx_output.send(machine.io().outputs.clone())?;

    //                 break;
    //             },
    //         }
            
    //     }

    //     if (i+1) % 100 == 0 {
    //         print!("\rTrace {}/{} done.", i+1, trace_count);
    //         io::stdout().flush().unwrap();
    //     }

    // }

    println!("");

    Ok(())

}

