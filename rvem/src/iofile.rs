/*
 * File: iofile.rs
 * Project: src
 * Created Date: Wednesday April 21st 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 2:23:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io;
use byteorder::{WriteBytesExt, BigEndian, LittleEndian};
use std::num::Wrapping;

pub struct IOFile {
    trace_len: u32,
    trace_count: u32,
    data: Vec<u8>,
}

impl IOFile {
    pub fn new() -> IOFile {
        IOFile {
            trace_len: 0,
            trace_count: 0,
            data: Vec::new(),
        }
    }

    pub fn push_trace(&mut self, trace: &Vec<u32>) {
        self.trace_len = trace.len() as u32 * 4;
        self.trace_count += 1;

        for word in trace.iter() {
            self.data.write_u32::<BigEndian>(*word).unwrap();
        }
    }

    pub fn push_trace_wrapping(&mut self, trace: &Vec<Wrapping<u32>>) {
        self.trace_len = trace.len() as u32 * 4;
        self.trace_count += 1;

        for word in trace.iter() {
            self.data.write_u32::<BigEndian>(word.0).unwrap();
        }
    }

    pub fn write_file<P: AsRef<Path>>(&self, path: P) -> io::Result<()> {
        let mut file = File::create(path)?;

        let mut tl = vec![];
        tl.write_u32::<LittleEndian>(self.trace_len).unwrap();

        let mut tc = vec![];
        tc.write_u32::<LittleEndian>(self.trace_count).unwrap();

        file.write(&tl)?;
        file.write(&tc)?;

        file.write_all(&self.data)?;

        Ok(())
    }
}