/*
 * File: trace_file.rs
 * Project: src
 * Created Date: Wednesday April 21st 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 3:07:38 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::io;
use byteorder::{WriteBytesExt, BigEndian, LittleEndian};
use std::num::Wrapping;

pub struct TraceFile {
    file: File,
    buffer: Vec<u8>
}

impl TraceFile {
    pub fn new<P: AsRef<Path>>(path: P) -> io::Result<TraceFile> {
        let file = File::create(path)?;
        

        let tf = TraceFile { file, buffer: Vec::new() };
        // tf.write_header(trace_len, trace_count)?;
        Ok(tf)
    }

    pub fn write_header(&mut self, trace_len: u32, trace_count: u32) -> io::Result<()> {
        let mut tl = vec![];
        tl.write_u32::<LittleEndian>(trace_len).unwrap();

        let mut tc = vec![];
        tc.write_u32::<LittleEndian>(trace_count).unwrap();

        self.file.write(&tl)?;
        self.file.write(&tc)?;

        Ok(())
    }

    pub fn push_trace(&mut self, trace: &Vec<u32>) {
        for word in trace.iter() {
            self.buffer.write_u32::<BigEndian>(*word).unwrap();
        }
    }

    pub fn push_trace_wrapping(&mut self, trace: &Vec<Wrapping<u32>>) {
        for word in trace.iter() {
            self.buffer.write_u32::<BigEndian>(word.0).unwrap();
        }
    }

    pub fn write_buffer(&mut self) -> io::Result<()> {
        self.file.write_all(&self.buffer)?;
        self.buffer.clear();
        Ok(())
    }

}