# RVEM: RISC-V emulator

## Usage

```
rvem -i ../aes/main.s -o io -t traces.binu8 trace -c 10000000
```

- i (input): choose the input program.
- o (output): the output name (will generate two files [name].in and [name].out).
- t (trace): trace file.
- c (count): how many executions.

- trace: to trace all instructions.
- run: to simply run the program.