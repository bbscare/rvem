/*
 * File: lib.rs
 * Project: src
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 10:13:38 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

#[macro_use] extern crate derive_more;
#[macro_use] extern crate log;

pub mod machine;
pub mod instructions;
pub mod error;
pub mod program;
pub mod security;