/*
 * File: error.rs
 * Project: src
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 19th February 2020 4:23:47 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::io;

use crate::{
    machine::*,
    instructions::*,
};
use snafu::{/*ensure, Backtrace, ErrorCompat, ResultExt,*/ Snafu};

#[derive(Debug, Snafu)]
pub enum MachineError {
    
    #[snafu(display("Error execution instruction at address {:x} (tick={}): {}", address, tick, source))]
    ExecutionError {
        #[snafu(source)]
        source:     ISAError,
        address:    MemoryAddress,
        tick:       u64,
    },

    #[snafu(display("IOError: {}", source))]
    IOMachineError {
        source: io::Error
    },


}

#[derive(Debug, Snafu)]
pub enum ISAError {
    #[snafu(display("{} cannot be parsed as a register name", input))]
    RegisterParsingError {
        input: String,
    },

    #[snafu(display("{} cannot be encoded", input))]
    EncodingError { 
        input: String
    },

    #[snafu(display("{} is out of bounds", val))]
    OutOfBoundsError {
        val: i64
    },

    #[snafu(display("Read at {:?} but it has never been written to", address))]
    IllegalRead {
        address: MemoryAddress
    },

    #[snafu(display("IOError: {}", source))]
    IOError {
        source: io::Error
    },

    #[snafu(display("Goblin (Elf/PE parser) error: {}", source_error))]
    GoblinError {
        source_error: goblin::error::Error
    },

    #[snafu(display("{}", message))]
    Any {
        message: String
    },

    #[snafu(display("0x{:x} is not a valid instruction", icode))]
    InvalidInstruction {
        icode: u32,
    },

    #[snafu(display("Impossible to rewrite {:?} for {:?}", part, inst))]
    ImpossibleRewritingError {
        inst: RV32I,
        part: InstructionPartSelector
    },

    #[snafu(display("CsrReadError: {:?}", csr))]
    CsrReadError {
        csr: Csr
    },

    #[snafu(display("CsrWriteError: {:?}", csr))]
    CsrWriteError {
        csr: Csr
    },
}

impl From<io::Error> for MachineError {
    fn from(source: io::Error) -> MachineError {
        MachineError::IOMachineError { source }
    }
}

impl From<io::Error> for ISAError {
    fn from(source: io::Error) -> ISAError {
        ISAError::IOError { source }
    }
}

impl From<goblin::error::Error> for ISAError {
    fn from(source_error: goblin::error::Error) -> ISAError {
        ISAError::GoblinError { source_error }
    }
}

impl From<String> for ISAError {
    fn from(message: String) -> ISAError {
        ISAError::Any { message }
    }
}