/*
 * File: binary_loader.rs
 * Project: program
 * Created Date: Tuesday July 16th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 19th July 2019 3:49:25 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use goblin::{
    Object,
};

use crate::{
    error::*,
    program::*,
    machine::*,
};

use std::{
    path::Path,
    fs::File,
    io::Read,
};

pub fn binfile2mem<P: AsRef<Path>>(start: MemoryAddress, filepath: P) -> Result<MemoryChunk, ISAError> {
    let mut fd = File::open(filepath)?;
    let mut buffer = Vec::new();
    fd.read_to_end(&mut buffer)?;

    Ok(MemoryChunk::new(start, buffer))
}

pub fn exefile2mem<P: AsRef<Path>>(filepath: P) -> Result<Vec<MemoryChunk>, ISAError> {
    let mut fd = File::open(filepath)?;
    let mut buffer = Vec::new();
    fd.read_to_end(&mut buffer)?;
    
    match Object::parse(&buffer)? {
        Object::Elf(elf) => {
            let mut chunks: Vec<MemoryChunk> = Vec::new();
            for pheader in elf.program_headers {//iterate and load segments
                let segment_range = pheader.file_range();
                let segment_buffer = Vec::from(&buffer[segment_range]);
                let start = MemoryAddress(pheader.p_vaddr);

                let mem_chunk = MemoryChunk::new(start, segment_buffer);
                chunks.push(mem_chunk);
            }

            Ok(chunks)
        },
        // Object::PE(pe) => {
        //     println!("pe: {:#?}", &pe);
        // },
        // Object::Mach(mach) => {
        //     println!("mach: {:#?}", &mach);
        // },
        // Object::Archive(archive) => {
        //     println!("archive: {:#?}", &archive);
        // },
        // Object::Unknown(magic) => { 
        //     println!("unknown magic: {:#x}", magic) 
        // }
        _ => { 
            return Err(ISAError::Any { message: format!("Only ELF files are supported." ) } ); 
        }
    }
}

#[test]
fn test_goblin() {
    let binary = exefile2mem("./resources/qsort.riscv");
    assert!(binary.is_ok());
}

#[test]
fn test_binloader() {
    let chunk = binfile2mem(MemoryAddress(0x80000000), "./resources/qsort.bin");
    assert!(chunk.is_ok());
}