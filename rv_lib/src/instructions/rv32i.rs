/*
 * File: rv32i.rs
 * Project: instructions
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 3:12:41 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use std::num::Wrapping;

use crate::{
    machine::*,
    instructions::{
        *,
        decoder::*,
    },
    error::ISAError,
    };

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum RV32I {
    //Integer computation
    Add(Register, Register, Register),
    Addi(Register, Register, Immediate),
    And(Register, Register, Register),
    Andi(Register, Register, Immediate),
    Sub(Register, Register, Register),
    Or(Register, Register, Register),
    Ori(Register, Register, Immediate),
    Xor(Register, Register, Register),
    Xori(Register, Register, Immediate),
    Sll(Register, Register, Register),
    Slli(Register, Register, Shamt),
    Sra(Register, Register, Register),
    Srai(Register, Register, Shamt),
    Srl(Register, Register, Register),
    Srli(Register, Register, Shamt),

    Lui(Register, Immediate),
    Auipc(Register, Immediate),
    Slt(Register, Register, Register),
    Slti(Register, Register, Immediate),
    Sltiu(Register, Register, Immediate),
    Sltu(Register, Register, Register),

    //Control transfer
    Beq(Register, Register, Offset),
    Bne(Register, Register, Offset),
    Bge(Register, Register, Offset),
    Bgeu(Register, Register, Offset),
    Blt(Register, Register, Offset),
    Bltu(Register, Register, Offset),
    Jal(Register, Offset),
    Jalr(Register, Register, Offset),

    //Load/Store
    Lb(Register, Register, Offset),
    Lbu(Register, Register, Offset),
    Lh(Register, Register, Offset),
    Lhu(Register, Register, Offset),
    Lw(Register, Register, Offset),
    Lwu(Register, Register, Offset),

    Sb(Register, Register, Offset),
    Sh(Register, Register, Offset),
    Sw(Register, Register, Offset),

    //Misc
    Fence(u8, u8),
    Fencei,
    Ebreak,//TraceOut
    Ecall,
    Csrrc(Register, Csr, Register),
    Csrrci(Register, Csr, Zimm),
    Csrrs(Register, Csr, Register),
    Csrrsi(Register, Csr, Zimm),
    Csrrw(Register, Csr, Register),
    Csrrwi(Register, Csr, Zimm),

}

impl Instruction for RV32I {
    type Reg = Wrapping<u32>; //wrapping: avoid overflow and get expected wrapping behavior

    fn apply<MEM: Memory + ConfidentialityKeyMemory>(&self, state: &mut State<Self::Reg>, mem: &mut MEM, inst_size: InstructionSize) -> Result<ApplicationEffect, ISAError> {
        
        let app_effect = match self {
            // Integer
            RV32I::Add(rd, rs1, rs2)                => apply::add(rd, rs1, rs2, state),
            RV32I::Addi(rd, rs1, imm)               => apply::addi(rd, rs1, imm, state),
            RV32I::And(rd, rs1, rs2)                => apply::and(rd, rs1, rs2, state),
            RV32I::Andi(rd, rs1, imm)               => apply::andi(rd, rs1, imm, state),
            RV32I::Sub(rd, rs1, rs2)                => apply::sub(rd, rs1, rs2, state),
            RV32I::Or(rd, rs1, rs2)                 => apply::or(rd, rs1, rs2, state),
            RV32I::Ori(rd, rs1, imm)                => apply::ori(rd, rs1, imm, state),
            RV32I::Xor(rd, rs1, rs2)                => apply::xor(rd, rs1, rs2, state),
            RV32I::Xori(rd, rs1, imm)               => apply::xori(rd, rs1, imm, state),
            RV32I::Sll(rd, rs1, rs2)                => apply::sll(rd, rs1, rs2, state),
            RV32I::Slli(rd, rs1, imm)               => apply::slli(rd, rs1, imm, state),
            RV32I::Srl(rd, rs1, rs2)                => apply::srl(rd, rs1, rs2, state),
            RV32I::Srli(rd, rs1, imm)               => apply::srli(rd, rs1, imm, state),
            RV32I::Sra(rd, rs1, rs2)                => apply::sra(rd, rs1, rs2, state),
            RV32I::Srai(rd, rs1, imm)               => apply::srai(rd, rs1, imm, state),

            RV32I::Lui(rd, imm)                     => apply::lui(rd, imm, state),
            RV32I::Auipc(rd, imm)                   => apply::auipc(rd, imm, state),
            RV32I::Slt(rd, rs1, rs2)                => apply::slt(rd, rs1, rs2, state),
            RV32I::Sltu(rd, rs1, rs2)               => apply::sltu(rd, rs1, rs2, state),
            RV32I::Slti(rd, rs1, imm)               => apply::slti(rd, rs1, imm, state),
            RV32I::Sltiu(rd, rs1, imm)              => apply::sltiu(rd, rs1, imm, state),


            // Load/Store
            RV32I::Lw(rd, rs1, off)                 => apply::lw(rd, rs1, off, state, mem)?,
            // RV32I::Lwu(rd, rs1, off)                => apply::lwu(rd, rs1, off, state, mem)?,
            RV32I::Lwu(_, rs1, _)                => apply::trace_out(rs1, state)?,
            RV32I::Lb(rd, rs1, off)                 => apply::lb(rd, rs1, off, state, mem)?,
            RV32I::Lbu(rd, rs1, off)                => apply::lbu(rd, rs1, off, state, mem)?,
            RV32I::Lh(rd, rs1, off)                 => apply::lh(rd, rs1, off, state, mem)?,
            RV32I::Lhu(rd, rs1, off)                => apply::lhu(rd, rs1, off, state, mem)?,

            RV32I::Sb(rs1, rs2, off)                => apply::sb(rs1, rs2, off, state, mem)?,
            RV32I::Sh(rs1, rs2, off)                => apply::sh(rs1, rs2, off, state, mem)?,
            RV32I::Sw(rs1, rs2, off)                => apply::sw(rs1, rs2, off, state, mem)?,

            

            // Control
            RV32I::Beq(rs1, rs2, off)               => apply::beq(rs1, rs2, off, state)?,
            RV32I::Bne(rs1, rs2, off)               => apply::bne(rs1, rs2, off, state)?,
            RV32I::Bge(rs1, rs2, off)               => apply::bge(rs1, rs2, off, state)?,
            RV32I::Blt(rs1, rs2, off)               => apply::blt(rs1, rs2, off, state)?,
            RV32I::Bgeu(rs1, rs2, off)              => apply::bgeu(rs1, rs2, off, state)?,
            RV32I::Bltu(rs1, rs2, off)              => apply::bltu(rs1, rs2, off, state)?,
            RV32I::Jal(rd, off)                     => apply::jal(rd, off, state, inst_size)?,
            RV32I::Jalr(rd, rs1, off)               => apply::jalr(rd, rs1, off, state, inst_size)?,

            // CSR

            RV32I::Csrrc(rd, csr, rs1)              => apply::csrrc(rd, rs1, csr, state)?,
            RV32I::Csrrci(rd, csr, zimm)            => apply::csrrci(rd, zimm, csr, state)?,
            RV32I::Csrrs(rd, csr, rs1)              => apply::csrrs(rd, rs1, csr, state)?,
            RV32I::Csrrsi(rd, csr, zimm)            => apply::csrrsi(rd, zimm, csr, state)?,
            RV32I::Csrrw(rd, csr, rs1)              => apply::csrrw(rd, rs1, csr, state)?,
            RV32I::Csrrwi(rd, csr, zimm)            => apply::csrrwi(rd, zimm, csr, state)?,

            // Misc

            RV32I::Ebreak                           => apply::ebreak(state)?,

            _ => { unimplemented!("{:?}", self) }
        };

        Ok(app_effect)
    }

}

impl Decode for RV32I {
    fn decode(icode: u32) -> Result<Self, ISAError> {
        //we do not check that we have a 32-bit instruction (C extension not supported) since the lower bits can be used for other purposes
        
        // if check32bit(icode) == false {
        //     return Err(ISAError::InvalidInstruction { icode });
        // }

        let instruction_encoding = InstructionEncoding::decode(icode)?;
        Ok(instruction_encoding.into())
    }
}

// fn check32bit(icode: u32) -> bool {
//     icode & 0x3 == 0x3
// }

impl Encode for RV32I {
    fn encode(&self) -> Result<u32, ISAError> {
        let encoding: InstructionEncoding = self.clone().into();
        encoding.encode() 
    }
}

impl From<RV32I> for InstructionEncoding {
    fn from(i: RV32I) -> InstructionEncoding {
        match i {
            RV32I::Lui(rd, imm)         => InstructionEncoding::U(UInst { tag: UTag::Lui, rd, imm }),
            RV32I::Auipc(rd, imm)       => InstructionEncoding::U(UInst { tag: UTag::Auipc, rd, imm }),
            RV32I::Jal(rd, off)         => InstructionEncoding::J(JInst { tag: JTag::Jal, rd, off }),
            RV32I::Jalr(rd, rs1, off)   => InstructionEncoding::I(IInst { tag: ITag::Jalr, rd, rs1, payload: IPayload::Offset(off) , zimm: Zimm(0)}),

            RV32I::Beq(rs1, rs2, off)   => InstructionEncoding::B(BInst { tag: BTag::Beq, rs1, rs2, off }),
            RV32I::Bne(rs1, rs2, off)   => InstructionEncoding::B(BInst { tag: BTag::Bne, rs1, rs2, off }),
            RV32I::Blt(rs1, rs2, off)   => InstructionEncoding::B(BInst { tag: BTag::Blt, rs1, rs2, off }),
            RV32I::Bge(rs1, rs2, off)   => InstructionEncoding::B(BInst { tag: BTag::Bge, rs1, rs2, off }),
            RV32I::Bltu(rs1, rs2, off)  => InstructionEncoding::B(BInst { tag: BTag::Bltu, rs1, rs2, off }),
            RV32I::Bgeu(rs1, rs2, off)  => InstructionEncoding::B(BInst { tag: BTag::Bgeu, rs1, rs2, off }),

            RV32I::Lb(rd, rs1, off)     => InstructionEncoding::I(IInst { tag: ITag::Lb, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),
            RV32I::Lh(rd, rs1, off)     => InstructionEncoding::I(IInst { tag: ITag::Lh, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),
            RV32I::Lw(rd, rs1, off)     => InstructionEncoding::I(IInst { tag: ITag::Lw, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),
            RV32I::Lbu(rd, rs1, off)    => InstructionEncoding::I(IInst { tag: ITag::Lbu, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),
            RV32I::Lhu(rd, rs1, off)    => InstructionEncoding::I(IInst { tag: ITag::Lhu, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),
            RV32I::Lwu(rd, rs1, off)    => InstructionEncoding::I(IInst { tag: ITag::Lwu, rd, rs1, payload: IPayload::Offset(off), zimm: Zimm(0) }),

            RV32I::Sb(rs1, rs2, off)    => InstructionEncoding::S(SInst { tag: STag::Sb, rs1, rs2, off }),
            RV32I::Sh(rs1, rs2, off)    => InstructionEncoding::S(SInst { tag: STag::Sh, rs1, rs2, off }),
            RV32I::Sw(rs1, rs2, off)    => InstructionEncoding::S(SInst { tag: STag::Sw, rs1, rs2, off }),

            RV32I::Addi(rd, rs1, imm)   => InstructionEncoding::I(IInst { tag: ITag::Addi, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Slti(rd, rs1, imm)   => InstructionEncoding::I(IInst { tag: ITag::Slti, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Sltiu(rd, rs1, imm)  => InstructionEncoding::I(IInst { tag: ITag::Sltiu, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Xori(rd, rs1, imm)   => InstructionEncoding::I(IInst { tag: ITag::Xori, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Ori(rd, rs1, imm)    => InstructionEncoding::I(IInst { tag: ITag::Ori, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Andi(rd, rs1, imm)   => InstructionEncoding::I(IInst { tag: ITag::Andi, rd, rs1, payload: IPayload::Immediate(imm), zimm: Zimm(0) }),
            RV32I::Slli(rd, rs1, shamt) => InstructionEncoding::I(IInst { tag: ITag::Slli, rd, rs1, payload: IPayload::Shamt(shamt), zimm: Zimm(0) }),
            RV32I::Srli(rd, rs1, shamt) => InstructionEncoding::I(IInst { tag: ITag::Srli, rd, rs1, payload: IPayload::Shamt(shamt), zimm: Zimm(0) }),
            RV32I::Srai(rd, rs1, shamt) => InstructionEncoding::I(IInst { tag: ITag::Srai, rd, rs1, payload: IPayload::Shamt(shamt), zimm: Zimm(0) }),

            RV32I::Add(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Add, rd, rs1, rs2 }),
            RV32I::Sub(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Sub, rd, rs1, rs2 }),
            RV32I::Sll(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Sll, rd, rs1, rs2 }),
            RV32I::Slt(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Slt, rd, rs1, rs2 }),
            RV32I::Sltu(rd, rs1, rs2)   => InstructionEncoding::R(RInst { tag: RTag::Sltu, rd, rs1, rs2 }),
            RV32I::Xor(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Xor, rd, rs1, rs2 }),
            RV32I::Srl(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Srl, rd, rs1, rs2 }),
            RV32I::Sra(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::Sra, rd, rs1, rs2 }),
            RV32I::Or(rd, rs1, rs2)     => InstructionEncoding::R(RInst { tag: RTag::Or, rd, rs1, rs2 }),
            RV32I::And(rd, rs1, rs2)    => InstructionEncoding::R(RInst { tag: RTag::And, rd, rs1, rs2 }),

            RV32I::Fence(pred, succ)    => InstructionEncoding::I(IInst { tag: ITag::Fence, rd: Register::X0, rs1: Register::X0, zimm: Zimm(0), payload: IPayload::Fence(pred, succ) }),
            RV32I::Fencei               => InstructionEncoding::I(IInst { tag: ITag::FenceI, rd: Register::X0, rs1: Register::X0, zimm: Zimm(0), payload: IPayload::Immediate(Immediate(0)) }),
            RV32I::Ecall                => InstructionEncoding::I(IInst { tag: ITag::Ecall, rd: Register::X0, rs1: Register::X0, zimm: Zimm(0), payload: IPayload::Immediate(Immediate(0)) }),
            RV32I::Ebreak               => InstructionEncoding::I(IInst { tag: ITag::Ebreak, rd: Register::X0, rs1: Register::X0, zimm: Zimm(0), payload: IPayload::Immediate(Immediate(0)) }),

            RV32I::Csrrw(rd, csr, rs1)  => InstructionEncoding::I(IInst { tag: ITag::Csrrw, rd, rs1, zimm: Zimm(0), payload: IPayload::Csr(csr) }),
            RV32I::Csrrs(rd, csr, rs1)  => InstructionEncoding::I(IInst { tag: ITag::Csrrs, rd, rs1, zimm: Zimm(0), payload: IPayload::Csr(csr) }),
            RV32I::Csrrc(rd, csr, rs1)  => InstructionEncoding::I(IInst { tag: ITag::Csrrc, rd, rs1, zimm: Zimm(0), payload: IPayload::Csr(csr) }),

            RV32I::Csrrwi(rd, csr, zimm) => InstructionEncoding::I(IInst { tag: ITag::Csrrwi, rd, rs1: Register::X0, zimm, payload: IPayload::Csr(csr) }),
            RV32I::Csrrsi(rd, csr, zimm) => InstructionEncoding::I(IInst { tag: ITag::Csrrsi, rd, rs1: Register::X0, zimm, payload: IPayload::Csr(csr) }),
            RV32I::Csrrci(rd, csr, zimm) => InstructionEncoding::I(IInst { tag: ITag::Csrrci, rd, rs1: Register::X0, zimm, payload: IPayload::Csr(csr) }),

            // _ => {unimplemented!()}
        }
    }
}

impl From<InstructionEncoding> for RV32I {
    fn from(encoding: InstructionEncoding) -> RV32I {
        match encoding {
            InstructionEncoding::U(uinst) => {
                match uinst.tag {
                    UTag::Lui       => RV32I::Lui(uinst.rd, uinst.imm),
                    UTag::Auipc     => RV32I::Auipc(uinst.rd, uinst.imm)
                }
            },

            InstructionEncoding::J(jinst) => {
                match jinst.tag {
                    JTag::Jal       => RV32I::Jal(jinst.rd, jinst.off),
                }
            },

            InstructionEncoding::R(rinst) => {
                match rinst.tag {
                    RTag::Add       => RV32I::Add(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::And       => RV32I::And(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Or        => RV32I::Or(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Sll       => RV32I::Sll(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Slt       => RV32I::Slt(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Sltu      => RV32I::Sltu(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Sra       => RV32I::Sra(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Srl       => RV32I::Srl(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Sub       => RV32I::Sub(rinst.rd, rinst.rs1, rinst.rs2),
                    RTag::Xor       => RV32I::Xor(rinst.rd, rinst.rs1, rinst.rs2),
                }
            },

            InstructionEncoding::I(iinst) => {
                match iinst.tag {
                    ITag::Addi      => RV32I::Addi(iinst.rd, iinst.rs1, iinst.payload.to_imm()),
                    ITag::Andi      => RV32I::Andi(iinst.rd, iinst.rs1, iinst.payload.to_imm()),
                    ITag::Csrrc     => RV32I::Csrrc(iinst.rd, iinst.payload.to_csr(), iinst.rs1),
                    ITag::Csrrci    => RV32I::Csrrci(iinst.rd, iinst.payload.to_csr(), iinst.zimm),
                    ITag::Csrrs     => RV32I::Csrrs(iinst.rd, iinst.payload.to_csr(), iinst.rs1),
                    ITag::Csrrsi    => RV32I::Csrrsi(iinst.rd, iinst.payload.to_csr(), iinst.zimm),
                    ITag::Csrrw     => RV32I::Csrrw(iinst.rd, iinst.payload.to_csr(), iinst.rs1),
                    ITag::Csrrwi    => RV32I::Csrrwi(iinst.rd, iinst.payload.to_csr(), iinst.zimm),
                    ITag::Ebreak    => RV32I::Ebreak,
                    ITag::Ecall     => RV32I::Ecall,
                    ITag::Fence     => {
                        let (pred, succ) = iinst.payload.to_fence();
                        RV32I::Fence(pred, succ)
                    },
                    ITag::FenceI    => RV32I::Fencei,
                    ITag::Jalr      => RV32I::Jalr(iinst.rd, iinst.rs1, iinst.payload.to_off()),

                    ITag::Lb        => RV32I::Lb(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Lbu       => RV32I::Lbu(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Lh        => RV32I::Lh(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Lhu       => RV32I::Lhu(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Lw        => RV32I::Lw(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Lwu       => RV32I::Lwu(iinst.rd, iinst.rs1, iinst.payload.to_off()),
                    ITag::Ori       => RV32I::Ori(iinst.rd, iinst.rs1, iinst.payload.to_imm()),
                    ITag::Slli      => RV32I::Slli(iinst.rd, iinst.rs1, iinst.payload.to_shamt()),
                    ITag::Slti      => RV32I::Slti(iinst.rd, iinst.rs1, iinst.payload.to_imm()),
                    ITag::Sltiu     => RV32I::Sltiu(iinst.rd, iinst.rs1, iinst.payload.to_imm()),
                    ITag::Srai      => RV32I::Srai(iinst.rd, iinst.rs1, iinst.payload.to_shamt()),
                    ITag::Srli      => RV32I::Srli(iinst.rd, iinst.rs1, iinst.payload.to_shamt()),
                    ITag::Xori      => RV32I::Xori(iinst.rd, iinst.rs1, iinst.payload.to_imm()),

                }
            },

            InstructionEncoding::B(binst) => {
                match binst.tag {
                    BTag::Beq       => RV32I::Beq(binst.rs1, binst.rs2, binst.off),
                    BTag::Bge       => RV32I::Bge(binst.rs1, binst.rs2, binst.off),
                    BTag::Bgeu      => RV32I::Bgeu(binst.rs1, binst.rs2, binst.off),
                    BTag::Blt       => RV32I::Blt(binst.rs1, binst.rs2, binst.off),
                    BTag::Bltu      => RV32I::Bltu(binst.rs1, binst.rs2, binst.off),
                    BTag::Bne       => RV32I::Bne(binst.rs1, binst.rs2, binst.off),
                }
            },

            InstructionEncoding::S(sinst) => {
                match sinst.tag {
                    STag::Sb        => RV32I::Sb(sinst.rs1, sinst.rs2, sinst.off),
                    STag::Sh        => RV32I::Sh(sinst.rs1, sinst.rs2, sinst.off),
                    STag::Sw        => RV32I::Sw(sinst.rs1, sinst.rs2, sinst.off),
                }
            },
        }
    }
}