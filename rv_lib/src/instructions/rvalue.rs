/*
 * File: value.rs
 * Project: instructions
 * Created Date: Monday July 1st 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 15th November 2019 3:25:39 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    error::ISAError,
    security::*,
};

use std::fmt;
use std::ops::*;
use std::num::Wrapping;


pub trait RValue: fmt::Debug + Clone + Copy + PartialEq + Eq + PartialOrd + Ord + Default + fmt::LowerHex
                    + From<MemoryAddress> + Into<MemoryAddress>
                    + Add<Output=Self> + Add<Immediate, Output=Self> + BitAnd<Output=Self> + BitAnd<Immediate, Output=Self> 
                    + BitOr<Output=Self> + BitOr<Immediate, Output=Self> + BitXor<Output=Self> + BitXor<Immediate, Output=Self>
                    + Shl<usize, Output=Self> + Shl<Immediate, Output=Self> + Shr<usize, Output=Self> + Shr<Immediate, Output=Self> 
                    + Not<Output=Self>
                    + Confidential
                    {

    type Signed: PartialOrd + Ord + Sub<Output=Self::Signed>;
    
    fn offset(&self, offset: &Offset) -> Result<Self, ISAError>;

    fn from_u32(val: u32) -> Self;
    fn from_u64(val: u64) -> Self;

    fn to_u8(&self)  -> u8;
    fn to_u16(&self) -> u16;
    fn to_u32(&self) -> u32;
    fn to_u64(&self) -> u64;
    fn to_usize(&self) -> usize {
        self.to_u64() as usize
    }

    fn to_signed(&self) -> Self::Signed;
    fn to_unsigned(s: &Self::Signed) -> Self;
    fn size() -> usize;
}


impl RValue for u64 {
    type Signed = i64;

    fn offset(&self, offset: &Offset) -> Result<u64, ISAError> {
        offset.offsetu64(*self)
    }

    fn from_u32(val: u32) -> u64 {
        val as u64
    }

    fn from_u64(val: u64) -> u64 {
        val
    }

    fn to_signed(&self) -> i64 {
        *self as i64
    }

    fn to_unsigned(i: &i64) -> u64 {
        *i as u64
    }

    fn size() -> usize {
        64
    }

    fn to_u8(&self)  -> u8 {
        (self & 0xFF) as u8
    }

    fn to_u16(&self) -> u16 {
        (self & 0xFFFF) as u16
    }

    fn to_u32(&self) -> u32 {
        (self & 0xFFFFFFFF) as u32
    }
     
    fn to_u64(&self) -> u64 {
        *self
    }
}


impl RValue for u32 {
    type Signed = i32;

    fn offset(&self, offset: &Offset) -> Result<u32, ISAError> {
        offset.offsetu32(*self)
    }

    fn from_u32(val: u32) -> u32 {
        val
    }

    fn from_u64(val: u64) -> u32 {
        (val & 0xFFFFFFFF) as u32
    }

    fn to_signed(&self) -> i32 {
        *self as i32
    }

    fn to_unsigned(i: &i32) -> u32 {
        *i as u32
    }

    fn size() -> usize {
        32
    }

    fn to_u8(&self)  -> u8 {
        (self & 0xFF) as u8
    }

    fn to_u16(&self) -> u16 {
        (self & 0xFFFF) as u16
    }

    fn to_u32(&self) -> u32 {
        *self
    }
     
    fn to_u64(&self) -> u64 {
        *self as u64
    }
}

impl RValue for Wrapping<u32> {
    type Signed = i32;

    fn offset(&self, offset: &Offset) -> Result<Wrapping<u32>, ISAError> {
        Ok(Wrapping(offset.offsetu32(self.0.to_u32())?))
    }

    fn from_u32(val: u32) -> Wrapping<u32> {
        Wrapping(val)
    }

    fn from_u64(val: u64) -> Wrapping<u32> {
        Wrapping((val & 0xFFFFFFFF) as u32)
    }

    fn to_signed(&self) -> Self::Signed {
        self.0.to_signed()
    }

    fn to_unsigned(i: &i32) -> Wrapping<u32> {
        Wrapping(*i as u32)
    }

    fn size() -> usize {
        32
    }

    fn to_u8(&self)  -> u8 {
        self.0.to_u8()
    }

    fn to_u16(&self) -> u16 {
        self.0.to_u16()
    }

    fn to_u32(&self) -> u32 {
        self.0.to_u32()
    }
     
    fn to_u64(&self) -> u64 {
        self.0.to_u64()
    }
}


// impl Add<Immediate> for u32 {
//     type Output = u32;

//     fn add(self, other: Immediate) -> u32 {
//         self.wrapping_add(other.0)
//     }
// }
