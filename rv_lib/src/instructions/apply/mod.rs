/*
 * File: mod.rs
 * Project: apply
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 4:49:16 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod application_effect;

//instructions
pub mod integer;
pub mod loadstore;
pub mod control;
pub mod csr;
pub mod hsc;
pub mod misc;

pub use self::application_effect::ApplicationEffect;
pub use self::integer::*;
pub use self::loadstore::*;
pub use self::control::*;
pub use self::csr::*;
pub use self::hsc::*;
pub use self::misc::*;