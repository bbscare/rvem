/*
 * File: hsc.rs
 * Project: apply
 * Created Date: Friday November 15th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 13th January 2020 2:43:50 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    error::ISAError,
    security::*,
};

pub fn hsc_create<R: RValue>(state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {

    //new key in clear
    state.hsc.next_key = NextKey::Clear(ConfidentialityKey::rand());
    
    Ok(ApplicationEffect::default())
}

pub fn hsc_finalize<R: RValue>(state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {

    match state.hsc.next_key {
        NextKey::Sealed(_) => {
            panic!("Finalizing a HSC creation not initialized with HSCcreate");//should interrupt
        },
        NextKey::Clear(ref mut key) => {
            *key = key.clone().encrypt(&state.hsc.context_key);
        }
    }
    
    Ok(ApplicationEffect::default())
}

pub fn hsc_switch<R: RValue>(rs1: &Register, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {

    let next_encrypted = match state.hsc.next_key {
        NextKey::Clear(_) => {
            panic!("Trying to switch to HSC not finalized.");//should interrupt
        },
        NextKey::Sealed(ref key) => key.clone()
    };

    let next_decrypted = next_encrypted.clone().decrypt(&state.hsc.context_key);
    let ep_decrypted = state.read_register(rs1).decrypt(&next_decrypted);

    let current_encrypted = state.hsc.context_key.clone().encrypt(&next_decrypted);
    
    state.hsc.next_key = NextKey::Sealed(current_encrypted);
    state.hsc.context_key = next_decrypted;
    
    Ok(ApplicationEffect::default().set_program_counter(ep_decrypted.into()))
}

pub fn hsc_load<R: RValue, MEM: ConfidentialityKeyMemory>(rs1: &Register, state: &mut State<R>, mem: &MEM) -> Result<ApplicationEffect, ISAError> {
    let address: MemoryAddress = state.read_register(rs1).into();
    let encrypted_key: ConfidentialityKey = mem.read_confidentiality_key(address)?;

    state.hsc.next_key = NextKey::Sealed(encrypted_key);
    
    Ok(ApplicationEffect::default())
}

pub fn hsc_store<R: RValue, MEM: ConfidentialityKeyMemory>(rs1: &Register, state: &mut State<R>, mem: &mut MEM) -> Result<ApplicationEffect, ISAError> {
    let address: MemoryAddress = state.read_register(rs1).into();

    match state.hsc.next_key {
        NextKey::Sealed(ref encrypted_key) => {
            mem.write_confidentiality_key(address, encrypted_key)?;
        },
        _ => { panic!("Trying to store secret key in clear."); } //should interrupt
    }
    
    Ok(ApplicationEffect::default())
}

pub fn hsc_bind<R: RValue>(rd: &Register, rs1: &Register, state: &mut State<R>) -> Result<ApplicationEffect, ISAError> {
    let ivalue = state.read_register(rs1);

    match state.hsc.next_key {
        NextKey::Clear(ref key) => {
            let ovalue = ivalue.encrypt(key);
            state.write_register(rd, ovalue);
        },
        _ => { panic!("Cannot bind without clear key."); }//should interrupt
    }

    Ok(ApplicationEffect::default())
}