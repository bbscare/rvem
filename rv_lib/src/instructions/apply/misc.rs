/*
 * File: misc.rs
 * Project: apply
 * Created Date: Tuesday April 20th 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 5:11:17 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    error::ISAError,
};

pub fn ebreak<R: RValue>(_state: &State<R>) -> Result<ApplicationEffect, ISAError> {
    
    trace!("EBREAK");
    Ok(ApplicationEffect::default())
    
}