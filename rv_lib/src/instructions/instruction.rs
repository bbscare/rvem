/*
 * File: instruction.rs
 * Project: instructions
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 22nd November 2019 10:36:32 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    instructions::decoder::*,
    error::ISAError,
    };

use std::fmt::Debug;

pub trait Instruction: Debug + Decode {
    type Reg: RValue;
    
    fn apply<MEM: Memory + ConfidentialityKeyMemory>(&self, state: &mut State<Self::Reg>, mem: &mut MEM, inst_size: InstructionSize) -> Result<ApplicationEffect, ISAError>;
    // fn decode32(icode: u32) -> Result<Self, ISAError> where Self: std::marker::Sized;
}