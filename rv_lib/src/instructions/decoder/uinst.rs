/*
 * File: uinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 2:59:39 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum UTag {
    Lui,
    Auipc,
}

impl Decode for UTag {
    fn decode(icode: u32) -> Result<UTag, ISAError> {
        let opcode: u8 = (icode & OPCODE_MASK) as u8;

        match opcode {
            0x34    => Ok(UTag::Lui),
            0x14    => Ok(UTag::Auipc),
            _       => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

impl Encode for UTag {
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            UTag::Lui   => Ok(0x34),
            UTag::Auipc => Ok(0x14),
        }
    }
}

#[derive(Debug,Clone)]
pub struct UInst {
    pub rd: Register,
    pub imm: Immediate,
    pub tag: UTag
}


impl Decode for UInst {
    fn decode(icode: u32) -> Result<UInst, ISAError> {
        let rd_val = (icode >> 7) & 0x1F;
        let rd = Register::decode(rd_val)?;
        let imm = Immediate(icode & 0xFFFFF000);//[31:12]
        let tag = UTag::decode(icode)?;

        Ok(UInst {
            rd,
            imm,
            tag
        })
        
    }
}

impl Encode for UInst {
    fn encode(&self) -> Result<u32, ISAError> {
        let imm = self.imm.0 & 0xFFFFF000;

        Ok(self.tag.encode()? | imm | (self.rd.encode()? << 7))
    }
}

#[test]
fn test_uinst() {
    let lui_icode = 558516;
    let lui_instr = UInst::decode(lui_icode).unwrap();
    assert_eq!(lui_instr.tag, UTag::Lui);
    assert_eq!(lui_instr.rd, Register::X11);
    assert_eq!(lui_instr.imm, Immediate(0x88000));

    let lui_reencode = lui_instr.encode().unwrap();
    assert_eq!(lui_reencode, lui_icode);
}