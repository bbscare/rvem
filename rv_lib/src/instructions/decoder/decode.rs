/*
 * File: decode.rs
 * Project: decoder
 * Created Date: Tuesday November 19th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 19th November 2019 11:57:00 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::error::*;

pub trait Decode {
    fn decode(icode: u32)-> Result<Self, ISAError> where Self: std::marker::Sized;
}