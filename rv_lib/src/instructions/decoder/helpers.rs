/*
 * File: helpers.rs
 * Project: decoder
 * Created Date: Monday November 18th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 18th November 2019 4:38:08 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub const OPCODE_MASK: u32 = 0x7C;