# Encoding for HSC instructions

Replacing JALR opcode: 11001XX
Rd and rs1 keep their place.
Sub-instructions are chosen according to bits 14:12 (3 bits):

- 000: HSCcreate
- 001: HSCbind
- 010: HSCfinalize
- 011: HSCswitch
- 100: HSCload
- 101: HSCstore