/*
 * File: uinst.rs
 * Project: decoder
 * Created Date: Wednesday July 17th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 3rd February 2020 2:40:50 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    error::*,
    instructions::decoder::*,
    instructions::decoder::helpers::*,
};

// use std::convert::TryFrom;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash,Copy)]
pub enum BTag {
    Beq,
    Bne,
    Blt,
    Bge,
    Bltu,
    Bgeu,
}

impl Decode for BTag {
    fn decode(icode: u32) -> Result<BTag, ISAError> {
        let opcode: u8 = (icode & OPCODE_MASK) as u8;
        let func3 = (icode & 0x7000) >> 12;

        if opcode != 0x60 {
            return Err(ISAError::InvalidInstruction { icode });
        }

        match func3 {
            0x0     => Ok(BTag::Beq),
            0x1     => Ok(BTag::Bne),
            0x4     => Ok(BTag::Blt),
            0x5     => Ok(BTag::Bge),
            0x6     => Ok(BTag::Bltu),
            0x7     => Ok(BTag::Bgeu),
            _       => Err(ISAError::InvalidInstruction { icode })
        }
    }
}

impl Encode for BTag { 
    fn encode(&self) -> Result<u32, ISAError> {
        match self {
            BTag::Beq   => Ok(0),
            BTag::Bne   => Ok(1),
            BTag::Blt   => Ok(4),
            BTag::Bge   => Ok(5),
            BTag::Bltu  => Ok(6),
            BTag::Bgeu  => Ok(7),
        }
    }
}

#[derive(Debug,Clone)]
pub struct BInst {
    pub rs1: Register,
    pub rs2: Register,
    pub off: Offset,
    pub tag: BTag
}


impl Decode for BInst {
    fn decode(icode: u32) -> Result<BInst, ISAError> {
        let rs1_val = (icode >> 15) & 0x1F;
        let rs1 = Register::decode(rs1_val)?;

        let rs2_val = (icode >> 20) & 0x1F;
        let rs2 = Register::decode(rs2_val)?;

        let imm_sign = (icode & 0x80000000 ) >> 19;
        let imm11 = ( icode & 0x80) << 4;
        let imm5 = (icode & 0x7E000000) >> 20;
        let imm1 = (icode & 0xF00) >> 7;

        // debug!("imm_sign {:x} |  imm_11 {:x} | imm_5 {:x} | imm_1 {:x}", imm_sign, imm11, imm5, imm1);

        let off = Offset::sign_extend(imm_sign | imm11 | imm5 | imm1, imm_sign, 12);
    
        let tag = BTag::decode(icode)?;

        Ok(BInst {
            rs1,
            rs2,
            off,
            tag
        })
        
    }
}

impl Encode for BInst {
    fn encode(&self) -> Result<u32, ISAError> {
        let opcode: u32 = 0x60;

        let imm = self.off.0 as u32;

        let imm7: u32 = imm & 0x1E | ((imm >> 11) & 1);
        let imm25: u32 = (imm >> 5) & 0x3F | (((imm >> 12) & 1) << 7); 

        Ok(opcode | (imm7 << 7) | (self.tag.encode()? << 12) | (self.rs1.encode()? << 15) | (self.rs2.encode()? << 20) | (imm25 << 25))
    }
}

#[test]
fn test_binst() {
    let beq_icode = 0x1c0a8460;
    let beq_iinst = BInst::decode(beq_icode).unwrap();

    assert_eq!(beq_iinst.tag, BTag::Beq);
    assert_eq!(beq_iinst.rs1, Register::X21);
    assert_eq!(beq_iinst.rs2, Register::X0);
    assert_eq!(beq_iinst.off, Offset(0x1C8));

    let reencode = beq_iinst.encode().unwrap();
    assert_eq!(beq_icode, reencode);
}