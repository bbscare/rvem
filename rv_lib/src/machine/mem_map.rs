/*
 * File: memory.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 22nd November 2019 11:00:06 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    // instructions::*,
    // security::*,
    // program::*,
    error::ISAError,
};

use std::collections::BTreeMap;
use std::fmt;

#[derive(Debug,Clone)]
pub struct MemoryMap {
    pub content: BTreeMap<MemoryAddress, u8>
}

impl Default for MemoryMap {
    fn default() -> MemoryMap {
        MemoryMap { content: BTreeMap::new() }
    }
}

impl Memory for MemoryMap {
    fn readu8(&self, add: MemoryAddress) -> Result<u8, ISAError> {
        match self.content.get(&add) {
            Some(b) => Ok(*b),
            None => Err(ISAError::IllegalRead { address: add })
        }
    }

    fn writeu8(&mut self, add: MemoryAddress, val: u8) -> Result<(), ISAError> {
        self.content.insert(add, val);
        Ok(())
    }

}

impl fmt::Display for MemoryMap {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Memory:")?;

        let bytes_per_line = 16;
        let header_mask = 0xFFFFFFFFFFFFFFF0u64;

        let mut last_header = 0x0u64;

        let mut drawn_on_line = 0;

        for (add, val) in self.content.iter() {

            if add.0 == 0 {
                write!(f, "\n\t@0x{:<10x}\t", add.0)?;
                last_header = add.0;
                drawn_on_line = 0;
            }

            //draw header if needed
            let address_header = add.0 & header_mask;
            if last_header != address_header{

                //finish line
                while drawn_on_line < bytes_per_line {
                    write!(f, ".. ")?;
                    drawn_on_line += 1;
                }

                //display address jump
                if (address_header - last_header) / bytes_per_line != 1 {
                    write!(f, "\n\t...")?;
                }

                //display address header
                write!(f, "\n\t@0x{:<10x}\t", address_header)?;
                last_header = address_header;
                drawn_on_line = 0;
            }


            let line_index = add.0 % bytes_per_line;

            //draw place holders
            while drawn_on_line < line_index {
                write!(f, ".. ")?;
                drawn_on_line += 1;
            }

            write!(f, "{:02x} ", val)?;
            drawn_on_line += 1;
        }

        //finish line
        while drawn_on_line < bytes_per_line {
            write!(f, ".. ")?;
            drawn_on_line += 1;
        }
        
        Ok(())
    }
}

#[test]
fn test_mem() {
    let mut mem = MemoryMap::default();

    assert!(mem.writeu32(MemoryAddress(0), (-1i32) as u32).is_ok());
    assert!(mem.writeu32(MemoryAddress(8), 0x55555555).is_ok());
    assert!(mem.writeu32(MemoryAddress(16), 0xaaaaaaaa).is_ok());

    assert!(mem.writeu64(MemoryAddress(63), (-2i64) as u64).is_ok());

    assert!(mem.writeu8(MemoryAddress(32), 0x12).is_ok());
    assert!(mem.writeu8(MemoryAddress(35), 0x34).is_ok());

    assert!(mem.writeu8(MemoryAddress(0x3000), 0x77).is_ok());

    // println!("{}", &mem);
}