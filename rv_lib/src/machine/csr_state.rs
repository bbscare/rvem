/*
 * File: configuration.rs
 * Project: machine
 * Created Date: Monday July 22nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 4:52:10 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rand::prelude::*;

use crate::{
    instructions::*,
    error::*,
};

pub const CYCLE: Csr        = Csr(0xC00);
pub const CYCLEH: Csr       = Csr(0xC80);
pub const TIME: Csr         = Csr(0xC01);
pub const TIMEH: Csr        = Csr(0xC81);
pub const INSTRET: Csr      = Csr(0xC02);
pub const INSTRETH: Csr     = Csr(0xC82);
pub const PRNG: Csr         = Csr(0x123);

#[derive(Debug,Clone)]
pub struct CSRState {
    //Control and Status Registers
    pub cycle:      u64,
    pub time:       u64,
    pub instret:    u64,
    rng:            ThreadRng,
}


impl CSRState {
    pub fn new() -> CSRState {
        let mut config = CSRState { 
            cycle:      0,
            time:       0,
            instret:    0,
            rng:        rand::thread_rng(),
        };
        config.reset();
        config
    }

    pub fn write32_csr(&mut self, csr: Csr, val: u32) -> Result<u32, ISAError> {
        let old = self.read32_csr(csr)?;

        match csr {
            CYCLE       => {
                self.cycle &= 0xFFFFFFFF00000000;
                self.cycle |= val as u64;
            },
            CYCLEH      => {
                self.cycle &= 0xFFFFFFFF;
                self.cycle |= (val as u64) << 32;
            },
            TIME       => {
                self.time &= 0xFFFFFFFF00000000;
                self.time |= val as u64;
            },
            TIMEH      => {
                self.time &= 0xFFFFFFFF;
                self.time |= (val as u64) << 32;
            },
            INSTRET       => {
                self.instret &= 0xFFFFFFFF00000000;
                self.instret |= val as u64;
            },
            INSTRETH      => {
                self.instret &= 0xFFFFFFFF;
                self.instret |= (val as u64) << 32;
            },
            PRNG        => {},//do nothing

            _ => { return Err(ISAError::CsrWriteError { csr: csr }); }
        }

        Ok(old)
    }

    pub fn read32_csr(&mut self, csr: Csr) -> Result<u32, ISAError> {
        match csr {
            CYCLE       => Ok((self.cycle & 0xFFFFFFFF) as u32),
            CYCLEH      => Ok((self.cycle >> 32 & 0xFFFFFFFF) as u32),
            TIME        => Ok((self.time & 0xFFFFFFFF) as u32),
            TIMEH       => Ok((self.time >> 32 & 0xFFFFFFFF) as u32),
            INSTRET     => Ok((self.instret & 0xFFFFFFFF) as u32),
            INSTRETH    => Ok((self.instret >> 32 & 0xFFFFFFFF) as u32),
            PRNG        => Ok(self.rng.gen()),

            _ => Err(ISAError::CsrReadError{ csr: csr })
        }
    }

    pub fn reset(&mut self) {
        self.cycle = 0;
        self.time = 0;
        self.instret = 0;
    }


    pub fn tick(&mut self) {
        self.cycle += 1;
        self.time += 1;
        self.instret += 1;
    }


    

    // pub fn cycle() -> Csr {
    //     Csr(0xC00)
    // }

    // pub fn cycleh() -> Csr {
    //     Csr(0xC80)
    // }

    // pub fn time() -> Csr {
    //     Csr(0xC01)
    // }

    // pub fn timeh() -> Csr {
    //     Csr(0xC81)
    // }

    // pub fn instret() -> Csr {
    //     Csr(0xC02)
    // }

    // pub fn instreth() -> Csr {
    //     Csr(0xC82)
    // }

    
}