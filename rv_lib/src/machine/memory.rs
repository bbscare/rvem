/*
 * File: memory.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 22nd November 2019 10:56:59 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    // instructions::*,
    security::*,
    program::*,
    error::ISAError,
};

pub trait Memory: Default {
    fn readu8(&self, add: MemoryAddress) -> Result<u8, ISAError>;
    
    fn readu16(&self, mut add: MemoryAddress) -> Result<u16, ISAError> {
        let mut buffer: [u8; 2] = [0u8;2];

        
        for i in 0..2 {
            buffer[i] = self.readu8(add)?;
            add.inc();
        }

        // unsafe { std::mem::transmute::<u32, [u8; 4]>(42u32.to_le()) } == [42, 0, 0, 0]
        Ok(unsafe { std::mem::transmute::<[u8; 2], u16>(buffer) }.to_le())
    }

    fn readu32(&self, mut add: MemoryAddress) -> Result<u32, ISAError> {
        let mut buffer: [u8; 4] = [0u8;4];

        
        for i in 0..4 {
            buffer[i] = self.readu8(add)?;
            add.inc();
        }

        // unsafe { std::mem::transmute::<u32, [u8; 4]>(42u32.to_le()) } == [42, 0, 0, 0]
        Ok(unsafe { std::mem::transmute::<[u8; 4], u32>(buffer) }.to_le())
    }

    fn readu64(&self, mut add: MemoryAddress) -> Result<u64, ISAError> {
        let mut buffer: [u8; 8] = [0u8;8];

        for i in 0..8 {
            buffer[i] = self.readu8(add)?;
            add.inc();
        }

        // unsafe { std::mem::transmute::<u32, [u8; 4]>(42u32.to_le()) } == [42, 0, 0, 0]
        Ok(unsafe { std::mem::transmute::<[u8; 8], u64>(buffer) }.to_le())
    }

    fn writeu8(&mut self, add: MemoryAddress, val: u8) -> Result<(), ISAError>;
    
    fn writeu16(&mut self, mut add: MemoryAddress, val: u16) -> Result<(), ISAError> {
        let buffer = unsafe { std::mem::transmute::<u16, [u8; 2]>(val.to_le()) };

        for i in 0..2 {
            self.writeu8(add, buffer[i])?;
            add.inc();
        }

        Ok(())
    }

    fn writeu32(&mut self, mut add: MemoryAddress, val: u32) -> Result<(), ISAError> {
        let buffer = unsafe { std::mem::transmute::<u32, [u8; 4]>(val.to_le()) };

        for i in 0..4 {
            self.writeu8(add, buffer[i])?;
            add.inc();
        }

        Ok(())
    }

    fn writeu64(&mut self, mut add: MemoryAddress, val: u64) -> Result<(), ISAError> {
        let buffer = unsafe { std::mem::transmute::<u64, [u8; 8]>(val.to_le()) };

        for i in 0..8 {
            self.writeu8(add, buffer[i])?;
            add.inc();
        }

        Ok(())
    }

    fn write_chunk(&mut self, chunk: MemoryChunk) -> Result<(), ISAError> {
        let mut add = chunk.start_address;

        for b in chunk.data {
            self.writeu8(add, b)?;
            add.inc();
        }

        Ok(())
    }

    fn read_chunk(&self, address: MemoryAddress, size: usize) -> Result<MemoryChunk, ISAError> {
        let mut current_address = address;
        let mut data: Vec<u8> = Vec::new();

        for _ in 0..size {
            let val = self.readu8(current_address).unwrap_or(0);
            current_address.inc();

            data.push(val);
        }

        Ok(MemoryChunk::new(address, data))
    }
}

pub trait ConfidentialityKeyMemory {
    fn read_confidentiality_key(&self, add: MemoryAddress) -> Result<ConfidentialityKey, ISAError>;
    fn write_confidentiality_key(&mut self, add: MemoryAddress, key: &ConfidentialityKey) -> Result<(), ISAError>;
}

impl<MEM: Memory> ConfidentialityKeyMemory for MEM {
    fn read_confidentiality_key(&self, mut add: MemoryAddress) -> Result<ConfidentialityKey, ISAError> {
        let mut buffer: [u32; KEY_WORD_SIZE] = [0u32; KEY_WORD_SIZE];

        for i in 0..KEY_WORD_SIZE {
            buffer[i] = self.readu32(add)?;
            add.offset(4);
        }

        Ok(ConfidentialityKey { material: buffer })
    }

    fn write_confidentiality_key(&mut self, mut add: MemoryAddress, key: &ConfidentialityKey) -> Result<(), ISAError>{
        for i in 0..KEY_WORD_SIZE {
            self.writeu32(add, key.material[i])?;
            add.offset(4);
        }
        Ok(())
    }
}