/*
 * File: state.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 4:49:59 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    instructions::*,
    machine::*,
    error::*,
};

use std::collections::BTreeMap;

#[derive(Debug,Clone)]
pub struct State<R: RValue> {
    pub registers: BTreeMap<Register, R>,
    pub csr: CSRState,
    pub hsc: HSCState,
    pub io: IOState<R>,
}

impl<R: RValue> State<R> {

    pub fn new() -> State<R> {
        State {
            registers:  BTreeMap::new(),
            csr:        CSRState::new(),
            hsc:        HSCState::new(),
            io:         IOState::new(),
        }
    }

    pub fn read_register(&self, reg: &Register) -> R {
        match reg {
            &Register::X0 => R::default(),
            r => self.registers.get(r).map(|r|*r).unwrap_or(R::default())
        }
        
    }

    pub fn write_register(&mut self, reg: &Register, val: R) {
        match reg {
            &Register::X0 => {},
            r => {self.registers.insert(*r, val);}
        }
    }

    pub fn apply_effect(&mut self, app_effect: ApplicationEffect, inst_size: InstructionSize) -> Result<(), ISAError> {
        
        //set PC to correct value (jump or increment)
        match app_effect.divert_program_counter {
            Some(add) => {
                self.write_register(&Register::PC, add.into());
            },
            None => {
                let new_pc = self.read_register(&Register::PC).offset(&inst_size.get_offset())?;
                self.write_register(&Register::PC, new_pc);
            }
        }
        

        Ok(())
    }
}