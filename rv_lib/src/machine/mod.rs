/*
 * File: mod.rs
 * Project: machine
 * Created Date: Tuesday July 2nd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Tuesday, 20th April 2021 5:17:23 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

pub mod address;
pub mod memory;
pub mod mem_map;
pub mod mem_array;
pub mod state;
pub mod instruction_size;
pub mod core;
pub mod csr_state;
pub mod hsc_state;
pub mod io_state;

pub use self::address::MemoryAddress;
pub use self::memory::{Memory, ConfidentialityKeyMemory};
pub use self::mem_map::MemoryMap;
pub use self::mem_array::MemoryArray1M;
pub use self::state::State;
pub use self::instruction_size::InstructionSize;
pub use self::core::Core;
pub use self::csr_state::{CSRState, PRNG};
pub use self::hsc_state::{HSCState, NextKey};
pub use self::io_state::{IOState, IOop};