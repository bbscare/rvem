/*
 * File: core.rs
 * Project: machine
 * Created Date: Friday July 5th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 21st April 2021 10:03:34 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::{
    machine::*,
    instructions::*,
    program::*,
    error::{
        *,
        MachineError::ExecutionError,
    },
};

use log::trace;

#[derive(Debug,Clone)]
pub struct Core<I: Instruction, MEM: Memory + ConfidentialityKeyMemory> {
    state:      State<I::Reg>,
    memory:     MEM,
}

impl<I: Instruction + Clone, MEM: Memory + ConfidentialityKeyMemory> Core<I, MEM> {
    pub fn new(start_address: MemoryAddress) -> Core<I, MEM> {
        let mut core = Core { 
            state:      State::new(),
            memory:     MEM::default(),
        };

        core.reset(start_address);
        core
    }

    pub fn reset(&mut self, start_address: MemoryAddress) {
        self.state = State::new();
        
        //set default start address
        self.state.write_register(&Register::PC, I::Reg::from_u32(start_address.into()));
    }

    fn fetch32(&self) -> Result<u32, ISAError> {
        let pc = self.state.read_register(&Register::PC);
        self.memory.readu32(pc.into())
    }

    pub fn step(&mut self, disassembler: Option<&mut Listing<I>>) -> Result<(), MachineError> {
        let pc: MemoryAddress = self.state.read_register(&Register::PC).into();
        let tick = self.state.csr.cycle;

        let icode = self.fetch32()
                            .map_err(|source| ExecutionError { address: pc, tick, source })?;
        let decoded = I::decode(icode)
                            .map_err(|source| ExecutionError { address: pc, tick, source })?;

        
        trace!("{}-> {:x}: {:08x} decoded as {:?}", tick, pc, icode, decoded);

        if let Some(dis) = disassembler {
            dis.register(pc, decoded.clone());
        }
        
        let app_effect = decoded.apply(&mut self.state, &mut self.memory, InstructionSize::Normal32)
                                .map_err(|source| ExecutionError { address: pc, tick, source })?;
        self.state.apply_effect(app_effect, InstructionSize::Normal32)
                    .map_err(|source| ExecutionError { address: pc, tick, source })?;
                    self.state.csr.tick();

        Ok(())
    }

    pub fn tracing_step(&mut self, disassembler: Option<&mut Listing<I>>) -> Result<Option<u32>, MachineError> {
        let pc: MemoryAddress = self.state.read_register(&Register::PC).into();
        let tick = self.state.csr.cycle;

        let icode = self.fetch32()
                            .map_err(|source| ExecutionError { address: pc, tick, source })?;
        let decoded = I::decode(icode)
                            .map_err(|source| ExecutionError { address: pc, tick, source })?;

        
        trace!("{}-> {:x}: {:08x} decoded as {:?}", tick, pc, icode, decoded);

        if let Some(dis) = disassembler {
            dis.register(pc, decoded.clone());
        }
        
        let app_effect = decoded.apply(&mut self.state, &mut self.memory, InstructionSize::Normal32)
                                .map_err(|source| ExecutionError { address: pc, tick, source })?;
                             
        let reg_value: Option<I::Reg> = app_effect.modified_register.map(|r| self.state.read_register(&r)); 
        self.state.apply_effect(app_effect, InstructionSize::Normal32)
                    .map_err(|source| ExecutionError { address: pc, tick, source })?;
                    self.state.csr.tick();

    

        Ok(reg_value.map(|rv|rv.to_u32()))
    }

    pub fn write_memory_chunk(&mut self, mem_chunk: MemoryChunk) -> Result<(), ISAError>{
        self.memory.write_chunk(mem_chunk)
    }

    pub fn read_memory_chunk(&self, start: MemoryAddress, size: usize) -> Result<MemoryChunk, ISAError> {
        self.memory.read_chunk(start, size)
    }

    pub fn run(&mut self) -> Result<(), MachineError> {
        loop {
            self.step(None)?;
        }
    }

    pub fn io(&self) -> IOState<I::Reg> {
        self.state.io.clone()
    }
}



// cannot pass with secure isa
#[test]
fn test_cores() {
    let _core32: Core<RV32I, MemoryArray1M> = Core::new(MemoryAddress(0));


    // let chunks = exefile2mem("./resources/qsort.riscv").unwrap();
    // for chunk in chunks {
    //     core32.write_memory_chunk(chunk);
    // }

    // for _ in 0..227220 {
    //     let step_res = core32.step();
    //     assert!(step_res.is_ok(), "Error: {}", step_res.err().unwrap());
    // }

    // println!("{}", core32.memory);
}