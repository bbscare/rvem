/*
 * File: parser.rs
 * Project: tests
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 3rd February 2020 2:56:21 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;


use simplass::parser::*;
// use badass::error::*;

use peg::{
  error::ParseError,
  str::LineCol,
};

use std::fmt;


fn compare_test_to_truth<T: Eq + fmt::Debug, PARSER: Fn(&str) -> Result<T, ParseError<LineCol>>>(test_truth: Vec<(&str, T)>, parser: PARSER) {
    for (test, truth) in test_truth.into_iter() {
        let test_res = parser(test);
        assert!(test_res.is_ok(), "{} cannot be parsed: {:?}", test, test_res);
        let unwraped_res = test_res.unwrap();
        assert_eq!(unwraped_res, truth, "{:?} instead of {:?} for {}", unwraped_res, truth, test);
    }
}
  
// fn check_fail<T: Eq + fmt::Debug, PARSER: Fn(&str) -> Result<T, ParseError<LineCol>>>(fail_vector: Vec<&str>, parser: PARSER) {
//     for test in fail_vector.into_iter() {
//         let res = parser(test);
//         assert!(res.is_err());
//     }
// }

#[test]
fn test_arithmetic_operations() {
  let test_vector = vec![
    " x2<- zero - x1 ;",
    "ra <- s5 <s s6;",
    "fp <- t5 << x24;",
  ];

  let truth = vec![
    RV32I::Sub(Register::X2, Register::X0, Register::X1),
    RV32I::Slt(Register::X1, Register::X21, Register::X22),
    RV32I::Sll(Register::X8, Register::X30, Register::X24),
  ];

  compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::arithmetic);
}


#[test]
fn test_arithmetic_immediate_operations() {
  let test_vector = vec![
    "x2<- zero - 5 ;",
    "ra <- s5 <s 0x45;",
    "fp <- t5 << b011;",
  ];

  let truth = vec![
    RV32I::Addi(Register::X2, Register::X0, Immediate::new(-5)),
    RV32I::Slti(Register::X1, Register::X21, Immediate::new(0x45)),
    RV32I::Slli(Register::X8, Register::X30, Shamt(3)),
  ];

  compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::arithmetic_immediate);
}

#[test]
fn test_store() {
    let test_vector = vec![
        "[sp] <- x3;",
        "[fp +0x45]h <- x5;",
        "[s7- 10]b <- a2;",
    ];

    let truth = vec![
        RV32I::Sw(Register::X2, Register::X3, Offset(0)),
        RV32I::Sh(Register::X8, Register::X5, Offset(0x45)),
        RV32I::Sb(Register::X23, Register::X12, Offset(-10)),
    ];

    compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::store);
}

#[test]
fn test_load() {
    let test_vector = vec![
        "sp <- [x3];",
        "fp <- [x5 + 0x45]h;",
        "s7 <- [a2- 10]b;",
    ];

    let truth = vec![
        RV32I::Lw(Register::X2, Register::X3, Offset(0)),
        RV32I::Lh(Register::X8, Register::X5, Offset(0x45)),
        RV32I::Lb(Register::X23, Register::X12, Offset(-10)),
    ];

    compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::load);
}

#[test]
fn test_load_symbol_address() {
    let test_vector = vec![
        "sp <- &foo ;",
    ];

    let truth = vec![
        vec![(RV32I::Lui(Register::X2, Immediate(0)), SymbolLink { symbol: AddressSymbol(format!("foo")), part: InstructionPartSelector::ImmU, pointer_kind: SymbolPointerKind::Absolute}),
        (RV32I::Addi(Register::X2, Register::X2, Immediate(0)), SymbolLink { symbol: AddressSymbol(format!("foo")), part: InstructionPartSelector::ImmI, pointer_kind: SymbolPointerKind::Absolute})],
    ];

    compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::load_symbol_address);
}

#[test]
fn test_branches() {
    let test_vector = vec![
        "if(x1 == x2) goto pc - 5;",
        "if ( sp <=s fp ) goto pc+0x46 ; ",
    ];

    let truth = vec![
        RV32I::Beq(Register::X1, Register::X2, Offset(-5)),
        RV32I::Bge(Register::X8, Register::X2, Offset(0x46)),
    ];

    compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::branch);
}


#[test]
fn test_data() {
  let test_vector = vec![
    "def d1: [u8] = [0, 1, 2, 3];",
    "def d2: i32 = -42;",
    "def s1: String = \"Hello World\";",
  ];

  let truth = vec![
    Data { name: format!("d1"), payload: DataPayload::Bytes(vec![0, 1, 2, 3]) },
    Data { name: format!("d2"), payload: DataPayload::I32(-42) },
    Data { name: format!("s1"), payload: DataPayload::String(format!("Hello World"))},
  ];

  compare_test_to_truth(test_vector.into_iter().zip(truth).collect(), simplass_parser::data);
}