/*
 * File: metatag.rs
 * Project: srv_isa
 * Created Date: Tuesday November 26th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 9th December 2019 2:38:43 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use std::str::FromStr;

// use crate::errors::*;

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum Metatag {

    ///The associated symbol is global and can be seen in the whole program
    Global,

    ///Specify address of this symbol
    Address(u64),
}

 

// impl FromStr for Metatag {
//     type Err = AssError;

//     fn from_str(s: &str) -> Result<Self, Self::Err> {
//         match s {
//             "global"    => Ok(Metatag::Global),
//             // "entry"     => Ok(Metatag::Entry),

//             _           => Err(AssError::MetatagParsingError { input: s.to_owned() })
//         }
//     }
// }