/*
 * File: selector.rs
 * Project: compiler
 * Created Date: Tuesday December 3rd 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 16th December 2019 2:17:09 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;

use crate::{
    parser::*,
    // errors::*,
};


#[derive(Debug, Clone,Copy, PartialEq,Eq, PartialOrd,Ord,Hash)]
pub enum SymbolPointerKind {
    /// When we need absolute addressing
    Absolute,
    /// When we need relative addressing (offset)
    Relative
}

#[derive(Debug, Clone, PartialEq,Eq, PartialOrd,Ord,Hash)]
pub struct SymbolLink {
    pub symbol:         AddressSymbol,
    pub part:           InstructionPartSelector,
    pub pointer_kind:   SymbolPointerKind,
}