/*
 * File: data.rs
 * Project: badass_isa
 * Created Date: Friday November 29th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Wednesday, 11th December 2019 4:28:41 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use byteorder::{/*ReadBytesExt, */WriteBytesExt, LittleEndian};

#[derive(Debug,Clone,PartialEq,Eq)]
pub enum DataPayload {
    Bytes(Vec<u8>),
    String(String),
    U32(u32),
    I32(i32),
}

impl DataPayload {
    pub fn byte_len(&self) -> usize {
        match self {
            DataPayload::Bytes(ref vec)     => vec.len(),
            DataPayload::String(ref s)      => s.len(),
            DataPayload::U32(_)             => 4,
            DataPayload::I32(_)             => 4,
        }
    }

    pub fn to_bytes(&self) -> Vec<u8> {
        match self {
            DataPayload::U32(u) => {
                let mut wtr: Vec<u8> = Vec::new();
                wtr.write_u32::<LittleEndian>(*u).unwrap();
                wtr
            },
            DataPayload::I32(i) => {
                let mut wtr: Vec<u8> = Vec::new();
                wtr.write_i32::<LittleEndian>(*i).unwrap();
                wtr
            },
            DataPayload::Bytes(vec) => vec.clone(),
            DataPayload::String(s) => {
                s.clone().into_bytes()
            },
        }
    }
}

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct Data {
    pub name:       String,
    pub payload:    DataPayload,
}