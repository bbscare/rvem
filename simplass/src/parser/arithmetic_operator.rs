/*
 * File: arithmetic_operation.rs
 * Project: srv_isa
 * Created Date: Monday November 25th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 6th December 2019 10:33:15 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use rv_lib::instructions::Register;

use std::str::FromStr;

use crate::errors::*;

// ********* PARSER

#[derive(Debug,Clone,PartialEq,Eq,PartialOrd,Ord,Hash)]
pub enum ArithmeticOperator {
    Add,
    Sub,
    Sll,
    Slt,
    Sltu,
    Xor,
    Srl,
    Sra,
    Or,
    And,
}

impl FromStr for ArithmeticOperator {
    type Err = AssError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "+" | "add"     => Ok(ArithmeticOperator::Add),
            "-" | "sub"     => Ok(ArithmeticOperator::Sub),
            "<<" | "sll"    => Ok(ArithmeticOperator::Sll),
            "^" | "xor"     => Ok(ArithmeticOperator::Xor),
            ">>" | "srl"    => Ok(ArithmeticOperator::Srl),
            "|" | "or"      => Ok(ArithmeticOperator::Or),
            "&" | "and"     => Ok(ArithmeticOperator::And),
            ">>s" | "sra"   => Ok(ArithmeticOperator::Sra),
            "<s" | "slt"    => Ok(ArithmeticOperator::Slt),
            "<u" | "sltu"   => Ok(ArithmeticOperator::Sltu),

            e => Err(AssError::ArithmeticOperatorParsingError { input: e.to_owned() })
        }
    }
} 