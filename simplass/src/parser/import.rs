/*
 * File: import.rs
 * Project: badass_isa
 * Created Date: Friday November 29th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 29th November 2019 2:56:09 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use std::path::PathBuf;
// use std::fmt;

#[derive(Debug,Clone,PartialEq,Eq)]
pub struct ImportFile {
    pub path: String
}

// impl fmt::Display for ImportFile {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         write!(f, "{}", self.path.display())
//     }
// }