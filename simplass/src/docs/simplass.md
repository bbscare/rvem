# Simplass assembly

There are no user defined variables, only registers and memory can hold values.

## Imports

We can import other files at the start of the file.

```
import "../std.bad";
```

By default, a file is scope. Therefore, symbol not annotated as global are not visible.

## Registers

Registers can be used by their number **x1**, **x2**, ..., **x31** or by their short name **sp**, **t0**, ...

## Symbol declaration

A symbol is used to represent an address that will be automatically resolved by the compiler.

```
@foo:
```

The global metatag is used to allow access to the symbol from other files.

```
#[global]
@bar:
```

```
#[global,address(0x8000)]
@entry:
```

## Arithmetic operations

Assignement operator is **<-**.
Legal operations are: + (add), - (sub), << (sll), ^ (xor), >> (srl), | (or), & (and), >>s (sra), <s (slt), <u (sltu).


```
sp <- x5 xor x6;
sp <- x5 ^ x6;
```

## Arithmetic immediate operations

Similar to previous arithmetic operations but right operand is a literal.

```
sp <- x5 xor 0x67;
```

The immediate value (the literal) is a signed value on 12 bits.

## Load / Store

Load
```
sp <- [x2]w; // word
sp <- [x2];  // word
sp <- [x2]b; //byte
sp <- [x2]h; // half word
```

Store
```
[sp]w <- x2;
[ra + 0x45] <- fp;
```

Quick load: load from literal value, using the given register (just after the assignement operator) at tempory register.
```
sp <-t0 [0x450]h;
ra <-t0 [&ptr + 4];
```

Quick store
```
[0x456]b <-t0 x2;
```

Trace out: to store a value in the output buffer
```
trace x1;
```

## Immediates

Load symbol address
```
sp <- &foo;
```

Load immediate value (lui and addi as needed)
```
x4 <- 0x4578;
x5 <- -878;
```

Auipc
```
x6 <- pc - 58;
x7 <- pc + 0x453;
```

## Branches


Conditions can compare between registers and values. Comparison operators are: ==, !=, <s, >s, <=s, >=s, <u, >u, <=u, >=u.

Conditional jumps:
```
if (x3 <s -3) goto &foo;
if (x3 <s -3) goto pc + 0x45;
```

## Jumps

```
goto -0x45;
goto &foo;
call &bar;
call 0x45;
```

Indirect jumps:

```
icall[x2] x1 + 0x45; // jump to address in x1 + 0x45, store current next instruction in x2
icall x2; //jump to address in x2, use default x1 as link register.
return;// jump to x1 (default link register)
```



## CSR

```
CSR[0x456] <- ra;  //csrw csr, rs1 = csrrw x0, csr, rs1
ra <- CSR[0x456];  // csrr rd, csr = csrrs rd, csr, x0
```


## Data

To store data into the program.

```
def vector: [u8] = [0, 1, 2, 3];
def s: String = "Hello World";
def u: u32 = 0x4000;
def i: i32 = -7;
```