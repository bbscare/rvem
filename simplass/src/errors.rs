/*
 * File: errors.rs
 * Project: src
 * Created Date: Friday December 6th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 16th December 2019 3:43:18 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

// use crate::parser::*;
// use rv_lib::instructions::*;

use snafu::{/*ensure, Backtrace, ErrorCompat, ResultExt,*/ Snafu};

#[derive(Debug, Snafu)]
pub enum AssError {

    #[snafu(display("{} cannot be parsed as an arithmetic operator", input))]
    ArithmeticOperatorParsingError {
        input: String,
    },

    #[snafu(display("{} is not a valid metatag", input))]
    MetatagParsingError {
        input: String,
    },

    #[snafu(display("{} cannot be parsed as a branch condition", input))]
    BranchConditionParsingError {
        input: String,
    },
    
    #[snafu(display("Parser error: {}", input))]
    ParserError {
        input: String,
    },

    #[snafu(display("IO error: {}", input))]
    IOError {
        input: String,
    },

    #[snafu(display("Dispatcher cannot be created: {}", input))]
    DispatcherCreationError {
        input: String,
    },
}

#[derive(Debug, Snafu)]
pub enum CompilerError {
    #[snafu(display("Feature is not implemented: {}.", input))]
    NotImplementedError { input: String, },

    #[snafu(display("Symbol {} cannot be found.", input))]
    SymbolNotFound { input: String },

    #[snafu(display("Error with tokens: {}", input))]
    TokenError { input: String },

    #[snafu(display("File not found: {}", input))]
    FileNotFoundError {
        input: String,
    },

    #[snafu(display("Layout error: {}", input))]
    LayoutError {
        input: String,
    },
}