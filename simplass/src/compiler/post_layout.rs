/*
 * File: layout.rs
 * Project: compiler
 * Created Date: Tuesday December 10th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Friday, 11th June 2021 11:46:33 am
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use log::*;

use crate::{
    compiler::*,
    parser::*,
    errors::*,
};

use rv_lib::{
    program::*,
};

use std::collections::{HashMap/*, BTreeMap*/};
use std::ops::{Range/*, RangeBounds*/};
use std::path::Path;

// use indexmap::IndexMap;

#[derive(Debug, Clone)]
pub struct PostLayout {
    pub chunks: Vec<MemoryChunk>,
    pub ledger: PostLayoutSymbolLedger,
}


impl PostLayout {

    pub fn compile<P: AsRef<Path>>(path: P, start: Option<MemoryAddress>) -> Result<PostLayout, CompilerError> {
        let tledger = TokenLedger::from_file(path)?;
        let prelayout = PreLayout::from_token_ledger(tledger);
        // trace!("PreLayout: {:?}", prelayout);
        let mut postlayout = PostLayout::from_pre_layout(start.unwrap_or(MemoryAddress(0)), prelayout)?;
        postlayout.apply_rewriting()?;
        // trace!("PostLayout: {:?}", postlayout);

        return Ok(postlayout);
    }

    /// Return true in the abscence of collision
    fn is_chunks_compliant(this_chunk: &MemoryChunk, other_chunk: &Vec<MemoryChunk>) -> bool {
        let this_start = this_chunk.start_address;
        let this_end = this_chunk.end_address();

        for other in other_chunk.iter() {
            if other.is_address_in_chunk(this_start) || other.is_address_in_chunk(this_end) {
                return false;
            }
        }

        true
    }

    /// Converting a PreLayout to a PostLayout triggers the layout application
    pub fn from_pre_layout(min_address: MemoryAddress, prelayout: PreLayout) -> Result<PostLayout, CompilerError> {
        let mut chunks: Vec<MemoryChunk> = Vec::new();
        let mut ledger: PostLayoutSymbolLedger = PostLayoutSymbolLedger::new();
        let mut pre2post_mappings: Vec<PreToPostLayoutMapping> = Vec::new();
        let mut current_address: MemoryAddress = min_address;
        let mut files: Vec<String> = Vec::new();

        let (sledger, parsed_files) = (prelayout.ledger, prelayout.parsed_files);

        for (path, mut parsed_file) in parsed_files.into_iter() {
            

            let mut current_idata_index: usize = 0;
            //pivots
            let pivots = sledger.get_explicitely_placed_symbols_by_file(&path);
            

            for (psymbol_inst_index, (_psymbol_path, paddress)) in pivots.iter() {//sorted by psymbol_inst_index (instruction_data index in parsed file)

                

                //chunck before pivot
                let chunk_icount = *psymbol_inst_index - current_idata_index;
                let (new_mem_chunk, new_pre2post_mapping) = PostLayout::create_chunk(&mut parsed_file, current_idata_index, chunk_icount, current_address, &path);

                //verify compliance with previous chunks
                if PostLayout::is_chunks_compliant(&new_mem_chunk, &chunks) == false {
                    return Err(CompilerError::LayoutError { 
                        input: format!("Memory chunks collision: chunk from {:?} to (excluding) {:?}.", new_mem_chunk.start_address, new_mem_chunk.end_address()) 
                    });
                }

                //update current index and address
                current_idata_index = *psymbol_inst_index;
                current_address = *paddress;//prepare next chunk address

                

                //push chunk if not empty
                if new_mem_chunk.size() != 0 {
                    chunks.push(new_mem_chunk);
                    pre2post_mappings.push(new_pre2post_mapping);
                }
            }

            //place all the instruction data left into a last chunk
            let chunk_i_count = parsed_file.len();
            let (last_chunk, last_pre2post_mapping) = PostLayout::create_chunk(&mut parsed_file, current_idata_index, chunk_i_count, current_address, &path);
            
            //verify compliance
            if PostLayout::is_chunks_compliant(&last_chunk, &chunks) == false {
                return Err(CompilerError::LayoutError { input: format!("Memory chunks collision: chunk from {:?} to (excluding) {:?}.", last_chunk.start_address, last_chunk.end_address()) });
            }

            //push chunk if not empty
            let chunk_size = last_chunk.size();
            current_address.offset(chunk_size as i64);
            if chunk_size != 0 {
                chunks.push(last_chunk);
                pre2post_mappings.push(last_pre2post_mapping);
            }

            //save paths for 2nd iteration
            files.push(path);
        }



        //symbols
        for path in files.iter() {
            let symbol_descs = sledger.get_symbols_by_file(path);

            for (symbol_path, symbol_desc) in symbol_descs.iter() {
                let address = PostLayout::map_iindex2address(path, symbol_desc.point_to_index, &pre2post_mappings)?;
                ledger.add_symbol(symbol_path.to_owned(), address);
            }
            
        }

        //links
        //reiter all the file for links
        for path in files.iter() {

            //update post layout symbol ledger for this file
            let file_links: HashMap<String, Vec<PreLayoutSymbolLinkEntry>> = sledger.get_links_by_file(path);

            for (symbol_path, links) in file_links.iter() {
                for link in links.iter() {
                    let new_address = PostLayout::map_iindex2address(path, link.point_to_index, &pre2post_mappings).map_err(|e|CompilerError::LayoutError { input: format!("For {} in {}: {}", symbol_path, path, e)})?;
                    ledger.add_link(symbol_path.to_owned(), link.part, link.pointer_kind, new_address);
                }
            }
        }


        Ok(PostLayout { chunks, ledger })
    }

    fn range_min(r: &Range<usize>) -> usize {
        if r.start < r.end {
            r.start
        }
        else {
            r.end
        }
    }

    fn map_iindex2address(filepath: &str, iindex: usize, maps: &Vec<PreToPostLayoutMapping>) -> Result<MemoryAddress, CompilerError> {
        for m in maps.iter() {
            if m.filepath == filepath {
                if m.valid_index_range.contains(&iindex) {
                    let offset = iindex - PostLayout::range_min(&m.valid_index_range);
                    let mut address = m.post_layout_start_address;
                    address.offset(4i64 * (offset as i64));
                    return Ok(address);
                }
            }
        }

        Err(CompilerError::LayoutError { input: format!("Cannot find pre to post layout mapping for instruction {}.", iindex)})
    }

    fn create_chunk(parsed_file: &mut Vec<InstructionData>, current_index: usize, chunk_i_count: usize, start_address: MemoryAddress, filepath: &str) -> (MemoryChunk, PreToPostLayoutMapping) {
        //this is the instructions and data into this chunk

        let chunk_idata: Vec<InstructionData> = parsed_file.drain(0..chunk_i_count).collect();
        
        //encode corresponding code
        let mut encoded_chunck: Vec<u8> = Vec::new();

        for idata in chunk_idata.iter() {
            let mut encoded = idata.to_bytes();
            encoded_chunck.append(&mut encoded);
        }

        //record corresponding mapping
        let mapping = PreToPostLayoutMapping { 
            filepath: filepath.to_owned(), 
            valid_index_range: current_index .. current_index + chunk_i_count, 
            post_layout_start_address: start_address};


        (MemoryChunk::new(start_address, encoded_chunck), mapping)
    }

    fn find_chunk_index(chunk_ranges: &HashMap<Range<MemoryAddress>, usize>, address: MemoryAddress) -> Result<usize, CompilerError> {
        for (range, index) in chunk_ranges.iter() {
            if range.contains(&address) {
                return Ok(*index);
            }
        }

        Err(CompilerError::LayoutError { input: format!("No chunk found containing address {:?}", address) })
    }

    pub fn apply_rewriting(&mut self) -> Result<(), CompilerError> { //inneficient algorithm -> need new datastructs to make it efficient
        
        // 1 - get ranges covered by chunks
        let mut chunk_ranges: HashMap<Range<MemoryAddress>, usize> = HashMap::new(); // (range, chunk index)

        for (i, chunk) in self.chunks.iter().enumerate() {
            let range = chunk.start_address .. chunk.end_address();
            chunk_ranges.insert(range, i);
        }

        // 2 - get all symbol links
        for (symbol_path, link_vec) in self.ledger.links.iter() {
            for link in link_vec.iter() {
                let where_to_rewrite_address = link.address;
                let symbol_address = self.ledger.symbols.get(symbol_path).ok_or(CompilerError::LayoutError { input: format!("Symbol {} not found.", symbol_path)})?;

                let rewrite_value: u64 = match link.pointer_kind {
                    SymbolPointerKind::Absolute => symbol_address.0,
                    SymbolPointerKind::Relative => (symbol_address.0 as i64 - where_to_rewrite_address.0 as i64) as u64,
                };

                

                //get concerned chunk
                let chunk_index = PostLayout::find_chunk_index(&chunk_ranges, where_to_rewrite_address)?;
                let chunk = self.chunks.get_mut(chunk_index).ok_or(CompilerError::LayoutError { input: format!("Chunk {} not found", chunk_index)})?;

                info!("Rewriting {}: rewrite 0x{:x} ({:?}) at {:x} in chunk starting {:x}, symbol address {:x}", symbol_path, rewrite_value, link.pointer_kind,  where_to_rewrite_address, chunk.start_address, symbol_address);

                let offset_in_chunk: usize = (where_to_rewrite_address.0 - chunk.start_address.0) as usize;
                link.part.rewrite(&mut chunk.data[offset_in_chunk..offset_in_chunk+4], rewrite_value);
            }
        }

        Ok(())
        
    }


}


#[derive(Debug,Clone,PartialEq,Eq)]
pub struct PreToPostLayoutMapping {
    pub filepath:                   String,
    pub valid_index_range:          Range<usize>,
    pub post_layout_start_address:  MemoryAddress
}

impl PreToPostLayoutMapping {
    pub fn is_empty(&self) -> bool {
        self.valid_index_range.start == self.valid_index_range.end
    }
}