/*
 * File: pre_layout.rs
 * Project: compiler
 * Created Date: Thursday December 12th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 12th December 2019 2:31:30 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use crate::compiler::*;
use crate::parser::*;

use indexmap::IndexMap;

#[derive(Debug)]
pub struct PreLayout {
    pub ledger: PreLayoutSymbolLedger,
    pub parsed_files: IndexMap<String, Vec<InstructionData>>
}


impl PreLayout {

    pub fn from_token_ledger(mut tledger: TokenLedger) -> PreLayout {
        //1 - get pre layout symbol ledger
        let mut sledger = PreLayoutSymbolLedger::from_tokens(&mut tledger);

        //2 - Get instruction data
        let mut idata: IndexMap<String, Vec<InstructionData>> = IndexMap::new();

        for (path, tokens) in tledger.token_map.into_iter() {
            let parsed = Self::instructions_data_from_tokens(&mut sledger, &path, tokens);
            idata.insert(path, parsed);
        }

        PreLayout {
            ledger: sledger,
            parsed_files: idata
        }
    }

    pub fn instructions_data_from_tokens(ledger: &mut PreLayoutSymbolLedger, current_path: &str, tokens: Vec<Token>) -> Vec<InstructionData> {
        let mut instructions_data: Vec<InstructionData>  = Vec::new();
        let mut iindex = 0usize;

        for tok in tokens.into_iter() {
            match tok {
                Token::Import(_)  => {},//should not be present
                Token::Data(d)              => { 
                    instructions_data.push(InstructionData::Data(d)); 
                    iindex += 1;
                },
                Token::Instructions(ivec)   => { 
                    for (i, link_opt) in ivec.into_iter() {
                        instructions_data.push(InstructionData::Instruction(i));
                        if let Some(link) = link_opt {
                            ledger.add_link(current_path, iindex, &link);
                        }
                        iindex += 1;
                    }
                },
                Token::SymbolDeclaration(_) => {},//should not be present
            }
        }


        instructions_data
    }
}