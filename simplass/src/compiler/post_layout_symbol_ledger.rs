/*
 * File: post_layout_symbol_ledger.rs
 * Project: compiler
 * Created Date: Thursday December 12th 2019
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 16th December 2019 2:14:07 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2019 INRIA
 */

use rv_lib::instructions::*;

use crate::compiler::*;
use crate::parser::*;

use std::collections::HashMap;

#[derive(Debug,Clone)]
pub struct PostLayoutSymbolLedger {
    pub symbols: HashMap<String, MemoryAddress>,//the address of the symbol (string is symbol path)
    pub links: HashMap<String, Vec<PostLayoutSymbolDestination>>,//where the symbol (String = symbol path) should be written
}


#[derive(Debug, Clone, Copy, PartialEq,Eq, PartialOrd,Ord,Hash)]
pub struct PostLayoutSymbolDestination {
    pub part:           InstructionPartSelector,
    pub pointer_kind:   SymbolPointerKind,
    pub address:        MemoryAddress
}

impl PostLayoutSymbolLedger {
    pub fn new() -> PostLayoutSymbolLedger {
        PostLayoutSymbolLedger { symbols: HashMap::new(), links: HashMap::new() }
    }

    pub fn add_symbol(&mut self, symbol_path: String, symbol_address: MemoryAddress) {
        self.symbols.insert(symbol_path, symbol_address);
    }

    pub fn add_link(&mut self, symbol_path: String, part: InstructionPartSelector, pointer_kind: SymbolPointerKind, address: MemoryAddress) {
        let mut previous = match self.links.remove(&symbol_path) {
            Some(p) => p,
            None => Vec::new()
        };

        previous.push(PostLayoutSymbolDestination {
            part,
            pointer_kind,
            address
        });

        self.links.insert(symbol_path, previous);
    }
}